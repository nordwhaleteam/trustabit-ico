'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        queryInterface.createTable('unique_accesses', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            user_id: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            ip: {
                type: Sequelize.STRING
            },
            createdAt: Sequelize.DATE
        },
        {
            hooks: {
                beforeCreate: (record) => {
                    record.createdAt = new Date();
                }
            }
        })
    },

    down: (queryInterface, Sequelize) => {
        queryInterface.dropTable('unique_accesses');
    }
};
