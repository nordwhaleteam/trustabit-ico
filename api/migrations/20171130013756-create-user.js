'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('users', {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
      },
      email: {
        type: Sequelize.STRING,
        validate: {
          isEmail: {
            msg: "Sorry, wrong email format"
          },
          unique: async email => {
            await User.find({ where: { email: email } }).then((user) => {
              if (user) {
                throw new Error('This email already in use');
              }
            });
          },
        },
        defaultValue: ''
      },
      password: {
        type: Sequelize.TEXT('tiny'),
        validate: {
          len: {
            args: [5, 255],
            msg: "Min password length 5 characters",
          },
          checkEquality: async email => {
            await User.find({ where: { email: email } }).then((user) => {
              if (user) {
                throw new Error('This email already in use');
              }
            });
          }
        },
      },
      wallet: {
        type: Sequelize.STRING,
      },
      token: {
        type: Sequelize.STRING,
      },
      attempts: {
        type: Sequelize.INTEGER,
      },
      active: {
        type: Sequelize.INTEGER(1),
        defaultValue: false
      },
      createdAt: Sequelize.DATE,
      updatedAt: Sequelize.DATE,
      lastAttemptAt: Sequelize.DATE,
      loggedInAt: Sequelize.DATE,
    }, {
        tableName: 'users',
        hooks: {
          beforeCreate: (user) => {
            user.password = bcrypt.hashSync(user.password);
            user.createdAt = new Date();
          },
          beforeUpdate: async function (user) {
            ;
            if (!bcrypt.compareSync(user.password, user.previous('password')) && user.password !== user.previous('password')) {
              user.password = bcrypt.hashSync(user.password);
            }
            user.updatedAt = new Date();
          },
        }
      });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('users');
  }
};