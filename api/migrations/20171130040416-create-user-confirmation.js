'use strict';
module.exports = {
  up: function(queryInterface, Sequelize) {
    return queryInterface.createTable('users_confirmations', {
      id: {
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        validate: {
          isUUID: 4
        },
      },
      user_id: {
        type: Sequelize.INTEGER,
      },
      code: {
        type: Sequelize.TEXT,
      },
      type: {
        type: Sequelize.INTEGER
      },
      used: {
        type: Sequelize.BOOLEAN,
        defaultValue: false
      },
      createdAt: {
        type: Sequelize.DATE
      },
      expireAt: {
        type: Sequelize.DATE
      },
    }, {
        tableName: 'users_confirmations',
        hooks: {
          beforeCreate: (userConfirmation) => {
            userConfirmation.createdAt = new Date();
            if (userConfirmation.type === CONFIRMATION_ACTION_RESET) {
              userConfirmation.expireAt = new Date(new Date() + (24 * 60 * 60 * 1000));
            }
          },
        }
      });
  },
  down: function(queryInterface, Sequelize) {
    return queryInterface.dropTable('users_confirmations');
  }
};