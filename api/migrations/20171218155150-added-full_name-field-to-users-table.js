'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('users', 'full_name', {
      type: Sequelize.STRING,
      defaultValue: ''
    });
  },
  
  down: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn('full_name');
  }
};
