'use strict';

module.exports = {
	up: function (queryInterface, Sequelize) {
		queryInterface.createTable('contactus', {
			id: {
				type: Sequelize.INTEGER,
				primaryKey: true,
				autoIncrement: true
			},
			name: {
				type: Sequelize.TEXT,
				validate: {
					len: {
						args: [1, 255],
						msg: "Wrong name length",
					}
				}
			},
			email: {
				type: Sequelize.STRING,
				validate: {
					isEmail: {
						msg: "Sorry, wrong email format"
					}
				}
			},
			message: Sequelize.TEXT,
			createdAt: Sequelize.DATE
		},
		{
			hooks: {
				beforeCreate: (record) => {
					record.createdAt = new Date();
				}
			}
		})
	},

	down: function (queryInterface, Sequelize) {
		queryInterface.dropTable('contactus');
	}
};
