'use strict';

module.exports = {
  up: function (queryInterface, Sequelize) {
    return queryInterface.addColumn('users', 'referral', {
      type: Sequelize.INTEGER,
      allowNull: true,
      defaultValue: null
    });
  },
  
  down: function (queryInterface, Sequelize) {
    return queryInterface.removeColumn('full_name');
  }
};