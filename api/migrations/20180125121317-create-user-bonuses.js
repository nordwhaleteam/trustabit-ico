'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        queryInterface.createTable('user_bonuses', {
            id: {
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            from_user: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            to_user: {
                type: Sequelize.INTEGER,
                allowNull: false
            },
            amount: {
                type: Sequelize.FLOAT,
                allowNull: false, 
                defaultValue: 0
            },
            createdAt: Sequelize.DATE
        },
        {
            hooks: {
                beforeCreate: (record) => {
                    record.createdAt = new Date();
                }
            }
        })
    },

    down: (queryInterface, Sequelize) => {
        queryInterface.dropTable('user_bonuses');
    }
};
