'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.CONFIRMATION_ACTION_RESET = exports.CONFIRMATION_ACTION_REGISTER = exports.UserConfirmation = undefined;

var _sequelize = require('sequelize');

var _sequelize2 = _interopRequireDefault(_sequelize);

var _database = require('../database');

var _database2 = _interopRequireDefault(_database);

var _user = require('../models/user');

var _user2 = _interopRequireDefault(_user);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var CONFIRMATION_ACTION_REGISTER = 1;
var CONFIRMATION_ACTION_RESET = 2;

var UserConfirmation = _database2.default.define('users_confirmations', {
	id: {
		primaryKey: true,
		type: _sequelize2.default.UUID,
		defaultValue: _sequelize2.default.UUIDV4,
		validate: {
			isUUID: 4
		}
	},
	user_id: {
		type: _sequelize2.default.INTEGER
	},
	code: {
		type: _sequelize2.default.TEXT
	},
	type: {
		type: _sequelize2.default.INTEGER
	},
	used: {
		type: _sequelize2.default.BOOLEAN,
		defaultValue: false
	},
	createdAt: {
		type: _sequelize2.default.DATE
	},
	expireAt: {
		type: _sequelize2.default.DATE
	}
}, {
	tableName: 'users_confirmations',
	hooks: {
		beforeCreate: function beforeCreate(userConfirmation) {
			userConfirmation.createdAt = new Date();
			if (userConfirmation.type === CONFIRMATION_ACTION_RESET) {
				userConfirmation.expireAt = new Date(new Date() + 24 * 60 * 60 * 1000);
			}
		}
	}
});

UserConfirmation.belongsTo(_user2.default, { foreignKey: 'user_id', targetKey: 'id' });

exports.UserConfirmation = UserConfirmation;
exports.CONFIRMATION_ACTION_REGISTER = CONFIRMATION_ACTION_REGISTER;
exports.CONFIRMATION_ACTION_RESET = CONFIRMATION_ACTION_RESET;