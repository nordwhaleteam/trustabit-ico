'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _sequelize = require('sequelize');

var _sequelize2 = _interopRequireDefault(_sequelize);

var _database = require('../database');

var _database2 = _interopRequireDefault(_database);

var _bcryptjs = require('bcryptjs');

var _bcryptjs2 = _interopRequireDefault(_bcryptjs);

var _crypto = require('crypto');

var _crypto2 = _interopRequireDefault(_crypto);

var _moment = require('moment');

var _moment2 = _interopRequireDefault(_moment);

var _web = require('web3');

var _web2 = _interopRequireDefault(_web);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var USER_ACTIVE_STATUS_DISABLED = false;
var USER_ACTIVE_STATUS_ENABLED = true;

var MAX_LOGIN_ATTEMPTS = 10;
var WAIT_TIME_BETWEEN_ATTEMPTS = 20; //minutes


var User = _database2.default.define('users', {
	id: {
		type: _sequelize2.default.INTEGER,
		primaryKey: true,
		autoIncrement: true
	},
	email: {
		type: _sequelize2.default.STRING,
		validate: {
			isEmail: {
				msg: "Sorry, wrong email format"
			},
			unique: function () {
				var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(email) {
					return _regenerator2.default.wrap(function _callee$(_context) {
						while (1) {
							switch (_context.prev = _context.next) {
								case 0:
									_context.next = 2;
									return User.find({ where: { email: email } }).then(function (user) {
										if (user) {
											throw new Error('This email already in use');
										}
									});

								case 2:
								case 'end':
									return _context.stop();
							}
						}
					}, _callee, undefined);
				}));

				return function unique(_x) {
					return _ref.apply(this, arguments);
				};
			}()
		},
		defaultValue: ''
	},
	full_name: {
		type: _sequelize2.default.STRING,
		validate: {
			notEmpty: true
		},
		defaultValue: ''
	},
	password: {
		type: _sequelize2.default.TEXT('tiny'),
		validate: {
			len: {
				args: [6, 255],
				msg: "Min password length 5 characters"
			}
		},
		defaultValue: ''
	},
	wallet: {
		type: _sequelize2.default.STRING,
		validate: {
			isValid: function () {
				var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(wallet) {
					return _regenerator2.default.wrap(function _callee2$(_context2) {
						while (1) {
							switch (_context2.prev = _context2.next) {
								case 0:
									if (_web2.default.utils.isAddress(wallet)) {
										_context2.next = 2;
										break;
									}

									throw new Error('This wallet is not valid');

								case 2:
								case 'end':
									return _context2.stop();
							}
						}
					}, _callee2, undefined);
				}));

				return function isValid(_x2) {
					return _ref2.apply(this, arguments);
				};
			}(),
			unique: function () {
				var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(wallet) {
					return _regenerator2.default.wrap(function _callee3$(_context3) {
						while (1) {
							switch (_context3.prev = _context3.next) {
								case 0:
									_context3.next = 2;
									return User.find({ where: { wallet: wallet } }).then(function (user) {
										if (user) {
											throw new Error('This wallet already in use');
										}
									});

								case 2:
								case 'end':
									return _context3.stop();
							}
						}
					}, _callee3, undefined);
				}));

				return function unique(_x3) {
					return _ref3.apply(this, arguments);
				};
			}()
		}
	},
	token: {
		type: _sequelize2.default.STRING
	},
	attempts: {
		type: _sequelize2.default.INTEGER
	},
	active: {
		type: _sequelize2.default.INTEGER(1),
		defaultValue: false
	},
	createdAt: _sequelize2.default.DATE,
	updatedAt: _sequelize2.default.DATE,
	lastAttemptAt: _sequelize2.default.DATE,
	loggedInAt: _sequelize2.default.DATE,
	referral: {
		type: _sequelize2.default.INTEGER,
		defaultValue: null
	}
}, {
	tableName: 'users',
	hooks: {
		beforeCreate: function beforeCreate(user) {
			user.password = _bcryptjs2.default.hashSync(user.password);
			user.createdAt = new Date();
		},
		beforeUpdate: function () {
			var _ref4 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4(user) {
				return _regenerator2.default.wrap(function _callee4$(_context4) {
					while (1) {
						switch (_context4.prev = _context4.next) {
							case 0:
								if (!_bcryptjs2.default.compareSync(user.password, user.previous('password')) && user.password !== user.previous('password')) {
									user.password = _bcryptjs2.default.hashSync(user.password);
								}
								user.updatedAt = new Date();

							case 2:
							case 'end':
								return _context4.stop();
						}
					}
				}, _callee4, this);
			}));

			function beforeUpdate(_x4) {
				return _ref4.apply(this, arguments);
			}

			return beforeUpdate;
		}()
	}
});

User.prototype.authenticate = function () {
	var _ref5 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee5(password) {
		var result;
		return _regenerator2.default.wrap(function _callee5$(_context5) {
			while (1) {
				switch (_context5.prev = _context5.next) {
					case 0:
						console.log(_bcryptjs2.default.compareSync(password, this.password));
						result = _bcryptjs2.default.compareSync(password, this.password);

						if (!result) {
							_context5.next = 7;
							break;
						}

						_context5.next = 5;
						return this.updateAttributes({
							attempts: 0,
							loggedInAt: new Date()
						});

					case 5:
						_context5.next = 9;
						break;

					case 7:
						_context5.next = 9;
						return this.trackAttempt();

					case 9:
						return _context5.abrupt('return', result);

					case 10:
					case 'end':
						return _context5.stop();
				}
			}
		}, _callee5, this);
	}));

	return function (_x5) {
		return _ref5.apply(this, arguments);
	};
}();

User.prototype.loginIsAllowed = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee6() {
	var result, diff;
	return _regenerator2.default.wrap(function _callee6$(_context6) {
		while (1) {
			switch (_context6.prev = _context6.next) {
				case 0:
					result = true;
					diff = (0, _moment2.default)().diff((0, _moment2.default)(this.lastAttemptAt, 'YYYY-MM-DD HH:mm:ss'), 'minutes');

					if (!(this.attempts >= MAX_LOGIN_ATTEMPTS && WAIT_TIME_BETWEEN_ATTEMPTS >= diff)) {
						_context6.next = 6;
						break;
					}

					_context6.next = 5;
					return this.trackAttempt();

				case 5:
					result = false;

				case 6:
					return _context6.abrupt('return', result);

				case 7:
				case 'end':
					return _context6.stop();
			}
		}
	}, _callee6, this);
}));

User.prototype.trackAttempt = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee7() {
	return _regenerator2.default.wrap(function _callee7$(_context7) {
		while (1) {
			switch (_context7.prev = _context7.next) {
				case 0:
					_context7.next = 2;
					return this.updateAttributes({
						attempts: ++this.attempts,
						lastAttemptAt: new Date()
					});

				case 2:
				case 'end':
					return _context7.stop();
			}
		}
	}, _callee7, this);
}));

User.prototype.generateToken = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee8() {
	return _regenerator2.default.wrap(function _callee8$(_context8) {
		while (1) {
			switch (_context8.prev = _context8.next) {
				case 0:
					_context8.t0 = this;
					_context8.next = 3;
					return _crypto2.default.randomBytes(32).toString('hex');

				case 3:
					_context8.t1 = _context8.sent;
					_context8.t2 = {
						token: _context8.t1
					};
					_context8.next = 7;
					return _context8.t0.updateAttributes.call(_context8.t0, _context8.t2);

				case 7:
					return _context8.abrupt('return', this.token);

				case 8:
				case 'end':
					return _context8.stop();
			}
		}
	}, _callee8, this);
}));

User.prototype.toJSON = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee9() {
	return _regenerator2.default.wrap(function _callee9$(_context9) {
		while (1) {
			switch (_context9.prev = _context9.next) {
				case 0:
					return _context9.abrupt('return', {
						id: this.id,
						email: this.email,
						token: this.token,
						active: this.active,
						wallet: this.wallet,
						full_name: this.full_name
					});

				case 1:
				case 'end':
					return _context9.stop();
			}
		}
	}, _callee9, this);
}));

exports.default = User;