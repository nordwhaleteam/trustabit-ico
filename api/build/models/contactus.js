'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _sequelize = require('sequelize');

var _sequelize2 = _interopRequireDefault(_sequelize);

var _database = require('../database');

var _database2 = _interopRequireDefault(_database);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _database2.default.define('contactus', {
	id: {
		type: _sequelize2.default.INTEGER,
		primaryKey: true,
		autoIncrement: true
	},
	name: {
		type: _sequelize2.default.TEXT,
		validate: {
			len: {
				args: [1, 255],
				msg: "Wrong name length"
			}
		}
	},
	email: {
		type: _sequelize2.default.STRING,
		validate: {
			isEmail: {
				msg: "Sorry, wrong email format"
			}
		}
	},
	message: {
		type: _sequelize2.default.TEXT,
		validate: {
			notEmpty: {
				msg: "Sorry, this field is required"
			}
		}
	},
	createdAt: _sequelize2.default.DATE
}, {
	hooks: {
		beforeCreate: function beforeCreate(record) {
			record.createdAt = new Date();
		}
	}
});