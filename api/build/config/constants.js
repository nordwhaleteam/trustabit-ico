'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _merge = require('lodash/merge');

var _merge2 = _interopRequireDefault(_merge);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Default configuations applied to all environments
var defaultConfig = {
	env: process.env.NODE_ENV,
	get envs() {
		return {
			test: process.env.NODE_ENV === 'test',
			development: process.env.NODE_ENV === 'development',
			production: process.env.NODE_ENV === 'production'
		};
	},

	version: require('../../package.json').version,
	root: _path2.default.normalize(__dirname + '/../../..'),
	port: process.env.PORT || 4244,
	ip: process.env.IP || '0.0.0.0',
	apiPrefix: '/api', // Could be /api/resource or /api/v2/resource
	userRoles: ['guest', 'user', 'admin'],

	/**
  * Security configuation options regarding sessions, authentication and hashing
  */
	security: {
		sessionSecret: process.env.SESSION_SECRET || 'i-am-the-secret-key',
		sessionExpiration: process.env.SESSION_EXPIRATION || 60 * 60 * 24 * 7, // 1 week
		saltRounds: process.env.SALT_ROUNDS || 12
	},

	mailChimp: {
		apiKey: process.env.MAIL_CHIMP_KEY,
		listId: process.env.MAIL_CHIMP_LISTID
	},
	google: {
		key: process.env.GOOGLE_API_KEY,
		recaptcha: {
			privateKey: process.env.GOOGLE_RECAPTCHA_PRIVATE_KEY,
			publicKey: process.env.GOOGLE_RECAPTCHA_PUBLIC_KEY
		}
	}
};

// Recursively merge configurations
exports.default = (0, _merge2.default)(defaultConfig, {});