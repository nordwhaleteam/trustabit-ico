'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _config = require('../helpers/config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = {
	transport: {
		host: _config2.default.MAIL_HOST,
		port: _config2.default.MAIL_PORT,
		secure: true, // use SSL
		auth: {
			user: _config2.default.MAIL_LOGIN,
			pass: _config2.default.MAIL_PASS
		}
	},
	contact: {
		from: _config2.default.MAIL_LOGIN,
		to: _config2.default.MAIL_CONTACT
	}
};