'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

exports.default = function (data) {
	var result = void 0;
	var base = _baseResponse.baseResponse;

	if (data.err) {
		var errorsData = (0, _baseError2.default)(data.err);
		result = data.res.status(errorsData.status).json(errorsData.errors);
	} else {
		result = data.res.status(_baseResponse.RESPONSE_SUCCESS).json(base);
	}

	return result;
};

var _baseResponse = require('../baseResponse');

var _baseError = require('../baseError');

var _baseError2 = _interopRequireDefault(_baseError);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }