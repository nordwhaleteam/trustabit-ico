"use strict";

Object.defineProperty(exports, "__esModule", {
	value: true
});
var RESPONSE_SUCCESS = exports.RESPONSE_SUCCESS = 200;
var RESPONSE_INVALID_INPUT = exports.RESPONSE_INVALID_INPUT = 422;
var RESPONSE_INTERNAL_ERROR = exports.RESPONSE_INTERNAL_ERROR = 500;

var baseResponse = exports.baseResponse = {
	result: [],
	errors: []
};