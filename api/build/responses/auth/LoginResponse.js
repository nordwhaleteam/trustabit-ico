'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

exports.default = function (data) {
	var result = void 0;

	if (data.err) {
		var errorsData = (0, _baseError2.default)(data.err);
		result = data.res.status(errorsData.status).json(Object.assign(_baseResponse.baseResponse, { errors: errorsData.errors }));
	} else {
		console.log(data);
		result = data.res.status(_baseResponse.RESPONSE_SUCCESS).json(Object.assign(_baseResponse.baseResponse, data.result));
	}

	return result;
};

var _baseResponse = require('../baseResponse');

var _baseError = require('../baseError');

var _baseError2 = _interopRequireDefault(_baseError);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }