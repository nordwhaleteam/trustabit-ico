'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

exports.default = function (err, req, res, next) {

	var response = {};

	response.message = err.message || 'Internal Server Error.';

	if (_constants2.default.envs.development) {
		response.stack = err.stack;
	}

	response.errors = parseValidationErrors(err);
	response.status = response.errors ? _baseResponse.RESPONSE_INVALID_INPUT : _baseResponse.RESPONSE_INTERNAL_ERROR;

	return response;
};

var _constants = require('../config/constants');

var _constants2 = _interopRequireDefault(_constants);

var _baseResponse = require('./baseResponse');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var parseValidationErrors = function parseValidationErrors(err) {
	var errors = null;
	if (err.errors) {
		var _errors = err.errors;

		for (var type in _errors) {
			if (type in _errors) {
				_errors[_errors[type].path] = _errors[type].message;
			}
		}
	} else errors = err;
	return errors;
};