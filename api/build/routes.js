'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _main = require('./controllers/main.controller');

var _main2 = _interopRequireDefault(_main);

var _auth = require('./controllers/auth.controller');

var _auth2 = _interopRequireDefault(_auth);

var _errorHandler = require('./middleware/errorHandler');

var _errorHandler2 = _interopRequireDefault(_errorHandler);

var _accessControl = require('./middleware/access-control');

var _accessControl2 = _interopRequireDefault(_accessControl);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var routes = new _express.Router();

routes.get('/', _main2.default.index);
routes.get('/user', _accessControl2.default, _main2.default.getUser);

routes.get('/user_referrals/:id', _main2.default.getReferralsCount);
routes.get('/user_bonus/:id', _main2.default.getUserBonus);

routes.post('/main/contactus', _main2.default.contactus);
routes.get('/main/confirm', _main2.default.confirm);
routes.post('/main/subscribe', _main2.default.subscribe);
routes.post('/main/reset', _main2.default.reset);
routes.post('/main/register', _main2.default.register);
routes.post('/auth/login', _auth2.default.login);
routes.post('/setWallet', _accessControl2.default, _main2.default.setWallet);

routes.use(_errorHandler2.default);

exports.default = routes;