'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _sequelize = require('sequelize');

var _sequelize2 = _interopRequireDefault(_sequelize);

var _config = require('./helpers/config');

var _config2 = _interopRequireDefault(_config);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = new _sequelize2.default(_config2.default.DB_NAME, _config2.default.DB_USER, _config2.default.DB_PASS, {
	host: _config2.default.DB_HOST,
	dialect: _config2.default.DB_TYPE,
	define: {
		timestamps: false // true by default
	},
	pool: {
		max: 5,
		min: 0,
		idle: 10000
	}
});