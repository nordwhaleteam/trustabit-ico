'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});
exports.confirmUserPasswordReset = exports.requestUserPasswordReset = exports.confirmUserRegistration = exports.findReferrals = exports.findAll = exports.findOne = exports.updateUser = exports.createUser = undefined;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _user = require('../models/user');

var _user2 = _interopRequireDefault(_user);

var _user_confirmation = require('../models/user_confirmation');

var _mail = require('../helpers/mail');

var _mail2 = _interopRequireDefault(_mail);

var _buildUrl = require('build-url');

var _buildUrl2 = _interopRequireDefault(_buildUrl);

var _config = require('../helpers/config');

var _config2 = _interopRequireDefault(_config);

var _constants = require('../config/constants');

var _constants2 = _interopRequireDefault(_constants);

var _crypto = require('crypto');

var _crypto2 = _interopRequireDefault(_crypto);

var _index = require('sequelize/lib/errors/index');

var sequelizeError = _interopRequireWildcard(_index);

var _bcryptjs = require('bcryptjs');

var _bcryptjs2 = _interopRequireDefault(_bcryptjs);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var createUser = exports.createUser = function () {
	var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(params) {
		var userModel, confirmationModel;
		return _regenerator2.default.wrap(function _callee$(_context) {
			while (1) {
				switch (_context.prev = _context.next) {
					case 0:
						if (!(params.password !== params.password_repeat)) {
							_context.next = 2;
							break;
						}

						throw new sequelizeError.ValidationError(null, [{
							path: 'password_repeat',
							type: 'password_repeat',
							message: "Password mismatch"
						}]);

					case 2:
						userModel = new _user2.default((0, _extends3.default)({}, params));
						_context.next = 5;
						return userModel.save();

					case 5:
						confirmationModel = new _user_confirmation.UserConfirmation({
							user_id: userModel.id,
							type: _user_confirmation.CONFIRMATION_ACTION_REGISTER
						});
						_context.next = 8;
						return confirmationModel.save();

					case 8:
						_context.next = 10;
						return userModel.generateToken();

					case 10:
						_context.next = 12;
						return userModel.toJSON();

					case 12:
						return _context.abrupt('return', _context.sent);

					case 13:
					case 'end':
						return _context.stop();
				}
			}
		}, _callee, undefined);
	}));

	return function createUser(_x) {
		return _ref.apply(this, arguments);
	};
}();

var updateUser = exports.updateUser = function () {
	var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(user, params) {
		var userModel;
		return _regenerator2.default.wrap(function _callee2$(_context2) {
			while (1) {
				switch (_context2.prev = _context2.next) {
					case 0:
						_context2.next = 2;
						return _user2.default.findOne({
							where: { id: user.id }
						});

					case 2:
						userModel = _context2.sent;
						_context2.next = 5;
						return userModel.update(Object.assign({}, userModel, (0, _extends3.default)({}, params)));

					case 5:
						_context2.next = 7;
						return userModel.save();

					case 7:
						_context2.next = 9;
						return userModel.toJSON();

					case 9:
						return _context2.abrupt('return', _context2.sent);

					case 10:
					case 'end':
						return _context2.stop();
				}
			}
		}, _callee2, undefined);
	}));

	return function updateUser(_x2, _x3) {
		return _ref2.apply(this, arguments);
	};
}();

var findOne = exports.findOne = function () {
	var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(user) {
		var userModel;
		return _regenerator2.default.wrap(function _callee3$(_context3) {
			while (1) {
				switch (_context3.prev = _context3.next) {
					case 0:
						_context3.next = 2;
						return _user2.default.findOne({
							where: { id: user.id }
						});

					case 2:
						userModel = _context3.sent;
						_context3.next = 5;
						return userModel.toJSON();

					case 5:
						return _context3.abrupt('return', _context3.sent);

					case 6:
					case 'end':
						return _context3.stop();
				}
			}
		}, _callee3, undefined);
	}));

	return function findOne(_x4) {
		return _ref3.apply(this, arguments);
	};
}();

var findAll = exports.findAll = function () {
	var _ref4 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4() {
		var usersModel;
		return _regenerator2.default.wrap(function _callee4$(_context4) {
			while (1) {
				switch (_context4.prev = _context4.next) {
					case 0:
						_context4.next = 2;
						return _user2.default.findAll();

					case 2:
						usersModel = _context4.sent;
						_context4.next = 5;
						return usersModel;

					case 5:
						return _context4.abrupt('return', _context4.sent);

					case 6:
					case 'end':
						return _context4.stop();
				}
			}
		}, _callee4, undefined);
	}));

	return function findAll() {
		return _ref4.apply(this, arguments);
	};
}();

var findReferrals = exports.findReferrals = function () {
	var _ref5 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee5(user) {
		var result;
		return _regenerator2.default.wrap(function _callee5$(_context5) {
			while (1) {
				switch (_context5.prev = _context5.next) {
					case 0:
						_context5.next = 2;
						return _user2.default.findAndCountAll({
							where: { referral: user.id }
						});

					case 2:
						result = _context5.sent;
						return _context5.abrupt('return', result);

					case 4:
					case 'end':
						return _context5.stop();
				}
			}
		}, _callee5, undefined);
	}));

	return function findReferrals(_x5) {
		return _ref5.apply(this, arguments);
	};
}();

var confirmUserRegistration = exports.confirmUserRegistration = function () {
	var _ref6 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee7(params) {
		return _regenerator2.default.wrap(function _callee7$(_context7) {
			while (1) {
				switch (_context7.prev = _context7.next) {
					case 0:
						_user_confirmation.UserConfirmation.findOne({
							where: {
								id: params.id,
								used: false,
								type: _user_confirmation.CONFIRMATION_ACTION_REGISTER
							}
						}).then(function (confirmation) {
							if (confirmation) {
								confirmation.getUser().then(function () {
									var _ref7 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee6(user) {
										return _regenerator2.default.wrap(function _callee6$(_context6) {
											while (1) {
												switch (_context6.prev = _context6.next) {
													case 0:
														_context6.next = 2;
														return user.update(Object.assign({}, user, { active: true }));

													case 2:
														_context6.next = 4;
														return confirmation.update(Object.assign({}, confirmation, { used: true }));

													case 4:
													case 'end':
														return _context6.stop();
												}
											}
										}, _callee6, undefined);
									}));

									return function (_x7) {
										return _ref7.apply(this, arguments);
									};
								}());
							}
						});

					case 1:
					case 'end':
						return _context7.stop();
				}
			}
		}, _callee7, undefined);
	}));

	return function confirmUserRegistration(_x6) {
		return _ref6.apply(this, arguments);
	};
}();

var requestUserPasswordReset = exports.requestUserPasswordReset = function () {
	var _ref8 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee8(params) {
		var user, confirmationModel;
		return _regenerator2.default.wrap(function _callee8$(_context8) {
			while (1) {
				switch (_context8.prev = _context8.next) {
					case 0:
						_context8.next = 2;
						return _user2.default.findOne({
							where: { email: params.email }
						});

					case 2:
						user = _context8.sent;

						if (!user) {
							_context8.next = 16;
							break;
						}

						_context8.next = 6;
						return _user_confirmation.UserConfirmation.update({ used: true }, {
							where: {
								used: false,
								user_id: user.id
							}
						});

					case 6:
						confirmationModel = new _user_confirmation.UserConfirmation();

						confirmationModel.user_id = user.id;
						confirmationModel.code = _crypto2.default.randomBytes(32).toString('hex');
						confirmationModel.type = _user_confirmation.CONFIRMATION_ACTION_RESET;
						_context8.next = 12;
						return confirmationModel.save();

					case 12:
						_context8.next = 14;
						return (0, _mail2.default)().sendResetPasswordEmail({
							url: (0, _buildUrl2.default)(_config2.default.HOST_NAME, {
								path: 'reset.html',
								queryParams: {
									id: confirmationModel.id,
									code: confirmationModel.code
								}
							}),
							email: user.email
						});

					case 14:
						_context8.next = 17;
						break;

					case 16:
						throw new sequelizeError.ValidationError(null, [{
							path: 'email',
							type: 'email',
							message: "User with this email doesnt exist"
						}]);

					case 17:
					case 'end':
						return _context8.stop();
				}
			}
		}, _callee8, undefined);
	}));

	return function requestUserPasswordReset(_x8) {
		return _ref8.apply(this, arguments);
	};
}();

var confirmUserPasswordReset = exports.confirmUserPasswordReset = function () {
	var _ref9 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee9(params) {
		var confirmationModel, userModel;
		return _regenerator2.default.wrap(function _callee9$(_context9) {
			while (1) {
				switch (_context9.prev = _context9.next) {
					case 0:
						_context9.next = 2;
						return _user_confirmation.UserConfirmation.findOne({
							where: {
								id: params.id,
								code: params.code,
								type: _user_confirmation.CONFIRMATION_ACTION_RESET,
								used: false
							}
						});

					case 2:
						confirmationModel = _context9.sent;

						if (!(params.password !== params.password_repeat)) {
							_context9.next = 5;
							break;
						}

						throw new sequelizeError.ValidationError(null, [{
							path: 'password',
							type: 'password',
							message: "Password mismatch"
						}]);

					case 5:
						if (!confirmationModel) {
							_context9.next = 19;
							break;
						}

						_context9.next = 8;
						return confirmationModel.getUser();

					case 8:
						userModel = _context9.sent;

						if (!userModel) {
							_context9.next = 16;
							break;
						}

						_context9.next = 12;
						return userModel.validate({ fields: ['password'] });

					case 12:
						_context9.next = 14;
						return userModel.updateAttributes({ password: params.password });

					case 14:
						_context9.next = 16;
						return confirmationModel.updateAttributes({ used: true });

					case 16:
						return _context9.abrupt('return', userModel);

					case 19:
						throw new sequelizeError.ValidationError(null, [{
							path: 'message',
							type: 'message',
							message: "Link is invalid"
						}]);

					case 20:
					case 'end':
						return _context9.stop();
				}
			}
		}, _callee9, undefined);
	}));

	return function confirmUserPasswordReset(_x9) {
		return _ref9.apply(this, arguments);
	};
}();

exports.default = {
	createUser: createUser,
	confirmUserRegistration: confirmUserRegistration,
	requestUserPasswordReset: requestUserPasswordReset,
	confirmUserPasswordReset: confirmUserPasswordReset,
	updateUser: updateUser,
	findOne: findOne,
	findAll: findAll,
	findReferrals: findReferrals
};