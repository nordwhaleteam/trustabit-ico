'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _dotenv = require('dotenv');

var _dotenv2 = _interopRequireDefault(_dotenv);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var env = _dotenv2.default.config().parsed;
var blank = 'check_config';

exports.default = {
	DB_HOST: env.DB_HOST || blank,
	DB_NAME: env.DB_NAME || blank,
	DB_USER: env.DB_USER || blank,
	DB_PASS: env.DB_PASS || blank,
	DB_TYPE: env.DB_TYPE || blank,

	MAIL_HOST: env.MAIL_HOST || blank,
	MAIL_PORT: env.MAIL_PORT || blank,
	MAIL_LOGIN: env.MAIL_LOGIN || blank,
	MAIL_PASS: env.MAIL_PASS || blank,
	MAIL_CONTACT: env.MAIL_CONTACT || blank,

	HOST_NAME: env.HOST_NAME || blank
};