'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _fs = require('fs');

var _fs2 = _interopRequireDefault(_fs);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _handlebars = require('handlebars');

var _handlebars2 = _interopRequireDefault(_handlebars);

var _nodemailer = require('nodemailer');

var _nodemailer2 = _interopRequireDefault(_nodemailer);

var _mail = require('../config/mail');

var _mail2 = _interopRequireDefault(_mail);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (transport) {
  if (!transport) {
    transport = _mail2.default.transport;
  }
  var transporter = _nodemailer2.default.createTransport(transport);

  var sendContactUsMail = function sendContactUsMail(data) {
    var templatePath = _path2.default.resolve(__dirname, '../views/emails', 'contact.html');
    var template = _handlebars2.default.compile(_fs2.default.readFileSync(templatePath, 'utf8'));
    var subject = 'New request: ' + data.name;
    var html = template(data);
    var sendMailData = Object.assign({}, _mail2.default.contact, { subject: subject, html: html });

    return transporter.sendMail(sendMailData, function (err, info) {
      console.log(err);
    });
  };

  var sendRegistrationEmail = function sendRegistrationEmail(data) {
    var templatePath = _path2.default.resolve(__dirname, '../views/emails', 'register.html');
    var template = _handlebars2.default.compile(_fs2.default.readFileSync(templatePath, 'utf8'));
    var subject = 'Registration on TrustaBit';
    var html = template(data);

    var sendMailData = Object.assign({}, _mail2.default.contact, { subject: subject, html: html, to: data.email });

    return transporter.sendMail(sendMailData, function (err, info) {
      console.log(err);
    });
  };

  var sendResetPasswordEmail = function sendResetPasswordEmail(data) {
    var templatePath = _path2.default.resolve(__dirname, '../views/emails', 'reset.html');
    var template = _handlebars2.default.compile(_fs2.default.readFileSync(templatePath, 'utf8'));
    var subject = 'TrustaBit password reset';
    var html = template(data);

    var sendMailData = Object.assign({}, _mail2.default.contact, { subject: subject, html: html, to: data.email });

    transporter.sendMail(sendMailData, function (err, info) {
      console.log(err);
    });
  };

  var sendSubscribeEmail = function sendSubscribeEmail(data) {
    try {
      var templatePath = _path2.default.resolve(__dirname, '../views/emails', 'subscribe.html');
      var template = _handlebars2.default.compile(_fs2.default.readFileSync(templatePath, 'utf8'));
      var subject = 'TrustaBit. Subscription';
      var html = template(data);

      var sendMailData = Object.assign({}, _mail2.default.contact, { subject: subject, html: html, to: data.email });

      return transporter.sendMail(sendMailData, function (err, info) {
        console.log(err);
      });
    } catch (e) {
      console.log(e);
    }
  };

  return {
    sendContactUsMail: sendContactUsMail,
    sendRegistrationEmail: sendRegistrationEmail,
    sendResetPasswordEmail: sendResetPasswordEmail,
    sendSubscribeEmail: sendSubscribeEmail
  };
};