'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _base = require('./base.controller');

var _contactus = require('../models/contactus');

var _contactus2 = _interopRequireDefault(_contactus);

var _errorHandler = require('../middleware/errorHandler');

var _errorHandler2 = _interopRequireDefault(_errorHandler);

var _response = require('../middleware/response');

var _response2 = _interopRequireDefault(_response);

var _mail = require('../helpers/mail');

var _mail2 = _interopRequireDefault(_mail);

var _user = require('../helpers/user');

var _user2 = _interopRequireDefault(_user);

var _user_bonuses = require('../helpers/user_bonuses');

var _user_bonuses2 = _interopRequireDefault(_user_bonuses);

var _sequelize = require('sequelize');

var _sequelize2 = _interopRequireDefault(_sequelize);

var _confirmResponse = require('../responses/main/confirmResponse');

var _confirmResponse2 = _interopRequireDefault(_confirmResponse);

var _RegisterResponse = require('../responses/auth/RegisterResponse');

var _RegisterResponse2 = _interopRequireDefault(_RegisterResponse);

var _request = require('request');

var _request2 = _interopRequireDefault(_request);

var _constants = require('../config/constants');

var _constants2 = _interopRequireDefault(_constants);

var _validate = require('../helpers/validate');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MainController = function (_BaseController) {
	(0, _inherits3.default)(MainController, _BaseController);

	function MainController() {
		var _ref,
		    _this2 = this;

		var _temp, _this, _ret;

		(0, _classCallCheck3.default)(this, MainController);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = MainController.__proto__ || Object.getPrototypeOf(MainController)).call.apply(_ref, [this].concat(args))), _this), _this.contactUsWhiteList = ['name', 'email', 'message'], _this.registerWhiteList = ['full_name', 'email', 'password', 'password_repeat', 'referral', 'wallet'], _this.updateWhiteList = ['wallet'], _this.contactus = function () {
			var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(req, res, next) {
				var params, model, result, mail;
				return _regenerator2.default.wrap(function _callee$(_context) {
					while (1) {
						switch (_context.prev = _context.next) {
							case 0:
								_context.prev = 0;
								params = _this.filterParams(req.body, _this.contactUsWhiteList);
								model = new _contactus2.default((0, _extends3.default)({}, params));
								_context.next = 5;
								return model.save();

							case 5:
								result = _context.sent;
								_context.next = 8;
								return (0, _mail2.default)().sendContactUsMail(params);

							case 8:
								mail = _context.sent;
								return _context.abrupt('return', (0, _response2.default)(req, res, result));

							case 12:
								_context.prev = 12;
								_context.t0 = _context['catch'](0);
								return _context.abrupt('return', (0, _errorHandler2.default)(_context.t0, req, res, next));

							case 15:
							case 'end':
								return _context.stop();
						}
					}
				}, _callee, _this2, [[0, 12]]);
			}));

			return function (_x, _x2, _x3) {
				return _ref2.apply(this, arguments);
			};
		}(), _this.index = function () {
			var _ref3 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee2(req, res) {
				return _regenerator2.default.wrap(function _callee2$(_context2) {
					while (1) {
						switch (_context2.prev = _context2.next) {
							case 0:
								res.json({
									version: '0.0.1'
								});

							case 1:
							case 'end':
								return _context2.stop();
						}
					}
				}, _callee2, _this2);
			}));

			return function (_x4, _x5) {
				return _ref3.apply(this, arguments);
			};
		}(), _this.getUser = function () {
			var _ref4 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee3(req, res, next) {
				return _regenerator2.default.wrap(function _callee3$(_context3) {
					while (1) {
						switch (_context3.prev = _context3.next) {
							case 0:
								_context3.prev = 0;
								_context3.t0 = res;
								_context3.next = 4;
								return req.currentUser.toJSON();

							case 4:
								_context3.t1 = _context3.sent;
								return _context3.abrupt('return', _context3.t0.json.call(_context3.t0, _context3.t1));

							case 8:
								_context3.prev = 8;
								_context3.t2 = _context3['catch'](0);

								(0, _errorHandler2.default)(_context3.t2, req, res, next);

							case 11:
							case 'end':
								return _context3.stop();
						}
					}
				}, _callee3, _this2, [[0, 8]]);
			}));

			return function (_x6, _x7, _x8) {
				return _ref4.apply(this, arguments);
			};
		}(), _this.getReferralsCount = function () {
			var _ref5 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee4(req, res, next) {
				var result;
				return _regenerator2.default.wrap(function _callee4$(_context4) {
					while (1) {
						switch (_context4.prev = _context4.next) {
							case 0:
								_context4.prev = 0;
								_context4.next = 3;
								return _user2.default.findReferrals({ id: req.params.id });

							case 3:
								result = _context4.sent;
								return _context4.abrupt('return', res.status(200).json({ count: result.count }));

							case 7:
								_context4.prev = 7;
								_context4.t0 = _context4['catch'](0);

								(0, _errorHandler2.default)(_context4.t0, req, res, next);

							case 10:
							case 'end':
								return _context4.stop();
						}
					}
				}, _callee4, _this2, [[0, 7]]);
			}));

			return function (_x9, _x10, _x11) {
				return _ref5.apply(this, arguments);
			};
		}(), _this.getUserBonus = function () {
			var _ref6 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee5(req, res, next) {
				var result;
				return _regenerator2.default.wrap(function _callee5$(_context5) {
					while (1) {
						switch (_context5.prev = _context5.next) {
							case 0:
								_context5.prev = 0;
								_context5.next = 3;
								return _user_bonuses2.default.getBonusSum({ id: req.params.id });

							case 3:
								result = _context5.sent;
								return _context5.abrupt('return', res.status(200).json({ result: result }));

							case 7:
								_context5.prev = 7;
								_context5.t0 = _context5['catch'](0);

								(0, _errorHandler2.default)(_context5.t0, req, res, next);

							case 10:
							case 'end':
								return _context5.stop();
						}
					}
				}, _callee5, _this2, [[0, 7]]);
			}));

			return function (_x12, _x13, _x14) {
				return _ref6.apply(this, arguments);
			};
		}(), _this.register = function () {
			var _ref7 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee6(req, res, next) {
				var params, user;
				return _regenerator2.default.wrap(function _callee6$(_context6) {
					while (1) {
						switch (_context6.prev = _context6.next) {
							case 0:
								(0, _validate.validateRecaptcha)(req, res, next);
								_context6.prev = 1;
								params = _this.filterParams(req.body, _this.registerWhiteList);
								_context6.next = 5;
								return _user2.default.createUser((0, _extends3.default)({}, params));

							case 5:
								user = _context6.sent;

								res.json({ user: user });
								_context6.next = 12;
								break;

							case 9:
								_context6.prev = 9;
								_context6.t0 = _context6['catch'](1);

								(0, _errorHandler2.default)(_context6.t0, req, res, next);

							case 12:
							case 'end':
								return _context6.stop();
						}
					}
				}, _callee6, _this2, [[1, 9]]);
			}));

			return function (_x15, _x16, _x17) {
				return _ref7.apply(this, arguments);
			};
		}(), _this.setWallet = function () {
			var _ref8 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee7(req, res, next) {
				var params;
				return _regenerator2.default.wrap(function _callee7$(_context7) {
					while (1) {
						switch (_context7.prev = _context7.next) {
							case 0:
								_context7.prev = 0;
								params = _this.filterParams(req.body, _this.updateWhiteList);
								_context7.t0 = res;
								_context7.next = 5;
								return _user2.default.updateUser(req.currentUser, { wallet: params.wallet });

							case 5:
								_context7.t1 = _context7.sent;

								_context7.t0.json.call(_context7.t0, _context7.t1);

								_context7.next = 12;
								break;

							case 9:
								_context7.prev = 9;
								_context7.t2 = _context7['catch'](0);

								(0, _errorHandler2.default)(_context7.t2, req, res, next);

							case 12:
							case 'end':
								return _context7.stop();
						}
					}
				}, _callee7, _this2, [[0, 9]]);
			}));

			return function (_x18, _x19, _x20) {
				return _ref8.apply(this, arguments);
			};
		}(), _this.subscribe = function () {
			var _ref9 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee8(req, res, next) {
				var model;
				return _regenerator2.default.wrap(function _callee8$(_context8) {
					while (1) {
						switch (_context8.prev = _context8.next) {
							case 0:
								_context8.prev = 0;
								model = new _contactus2.default({
									email: req.body.email
								}, { skip: ['name', 'message'] });
								_context8.next = 4;
								return model.save();

							case 4:

								//this.addToMailchimp(req.body.email,'');

								/*if (appConfig.env === 'production') {
        	this.sendGForm(req.body);
        } else {
        	this.sendGFormTest(req.body);
        }*/

								(0, _mail2.default)().sendSubscribeEmail(req.body);
								return _context8.abrupt('return', (0, _response2.default)(req, res, {}));

							case 8:
								_context8.prev = 8;
								_context8.t0 = _context8['catch'](0);
								return _context8.abrupt('return', (0, _errorHandler2.default)(_context8.t0, req, res, next));

							case 11:
							case 'end':
								return _context8.stop();
						}
					}
				}, _callee8, _this2, [[0, 8]]);
			}));

			return function (_x21, _x22, _x23) {
				return _ref9.apply(this, arguments);
			};
		}(), _this.reset = function () {
			var _ref10 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee9(req, res, next) {
				var id, code, password, password_repeat, userModel;
				return _regenerator2.default.wrap(function _callee9$(_context9) {
					while (1) {
						switch (_context9.prev = _context9.next) {
							case 0:
								_context9.prev = 0;
								id = req.param('id');
								code = req.param('code');
								password = req.body.password;
								password_repeat = req.body.password_repeat;

								if (!(id && code)) {
									_context9.next = 19;
									break;
								}

								_context9.next = 8;
								return _user2.default.confirmUserPasswordReset({ id: id, code: code, password: password, password_repeat: password_repeat });

							case 8:
								userModel = _context9.sent;
								_context9.t0 = _RegisterResponse2.default;
								_context9.next = 12;
								return userModel.toJSON();

							case 12:
								_context9.t1 = _context9.sent;
								_context9.t2 = {
									user: _context9.t1
								};
								_context9.t3 = res;
								_context9.t4 = {
									result: _context9.t2,
									res: _context9.t3
								};
								return _context9.abrupt('return', (0, _context9.t0)(_context9.t4));

							case 19:
								_context9.next = 21;
								return _user2.default.requestUserPasswordReset({ email: req.body.email });

							case 21:
								return _context9.abrupt('return', (0, _response2.default)(req, res, {}));

							case 24:
								_context9.prev = 24;
								_context9.t5 = _context9['catch'](0);
								return _context9.abrupt('return', (0, _errorHandler2.default)(_context9.t5, req, res, next));

							case 27:
							case 'end':
								return _context9.stop();
						}
					}
				}, _callee9, _this2, [[0, 24]]);
			}));

			return function (_x24, _x25, _x26) {
				return _ref10.apply(this, arguments);
			};
		}(), _this.confirm = function () {
			var _ref11 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee10(req, res, next) {
				var response;
				return _regenerator2.default.wrap(function _callee10$(_context10) {
					while (1) {
						switch (_context10.prev = _context10.next) {
							case 0:
								response = null;
								_context10.prev = 1;
								_context10.next = 4;
								return _user2.default.confirmUserRegistration({
									id: req.param('id')
								});

							case 4:
								response = (0, _confirmResponse2.default)({ req: req, res: res, next: next });
								_context10.next = 10;
								break;

							case 7:
								_context10.prev = 7;
								_context10.t0 = _context10['catch'](1);

								response = (0, _confirmResponse2.default)({ err: _context10.t0, req: req, res: res, next: next });

							case 10:
								return _context10.abrupt('return', response);

							case 11:
							case 'end':
								return _context10.stop();
						}
					}
				}, _callee10, _this2, [[1, 7]]);
			}));

			return function (_x27, _x28, _x29) {
				return _ref11.apply(this, arguments);
			};
		}(), _this.sendGForm = function (req) {
			try {
				// request.post('https://docs.google.com/forms/d/e/1FAIpQLScjf-sJzxgIJ_yCFnRQMeL5EIkHwhgMREaZdvLMSTwV1fEYOw/formResponse', {
				// 	form: {
				// 		'entry.403294950': req.email ? req.email : '',
				// 		'entry.770328930': req.utm_source ? req.utm_source : '',
				// 		'entry.1668652964': req.utm_medium ? req.utm_medium : '',
				// 		'entry.355906419': req.utm_campaign ? req.utm_campaign : '',
				// 		'entry.760235735': req.utm_content ? req.utm_content : '',
				// 		'entry.1395438923': req.utm_term ? req.utm_term : '',
				// 		'entry.1062312962': req.reflink ? req.reflink : ''
				// 	}
				// });
			} catch (e) {
				console.log(e);
			}
		}, _this.sendGFormTest = function (req) {
			try {
				//old test google form (modultraid)
				_request2.default.post('https://docs.google.com/forms/d/e/1FAIpQLSetgWw-jdtcnPt6327tV14-mrnzGEiW47fN4iUtcJPhSeiKXA/formResponse', {
					form: (0, _defineProperty3.default)({
						'entry.282765473': req.email ? req.email : '',
						'entry.1916199099': req.utm_source ? req.utm_source : '',
						'entry.1600229878': req.utm_medium ? req.utm_medium : '',
						'entry.595357331': req.utm_campaign ? req.utm_campaign : '',
						'entry.1447209678': req.utm_content ? req.utm_content : '',
						'entry.1560038263': req.utm_term ? req.utm_term : ''
					}, 'entry.1560038263', req.reflink ? req.reflink : '')
				});
			} catch (e) {
				console.log(e);
			}
		}, _this.addToMailchimp = function (email, name) {
			try {
				var dataCenter = _constants2.default.mailChimp.apiKey.split('-')[1];
				(0, _request2.default)({
					url: 'https://' + dataCenter + '.api.mailchimp.com/3.0/lists/' + _constants2.default.mailChimp.listId + '/members',
					json: {
						'email_address': email,
						'status': 'subscribed',
						'merge_fields': {
							'FNAME': name
						}
					},
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						'Authorization': 'apikey ' + _constants2.default.mailChimp.apiKey
					}
				}, function (error, response, body) {
					if (error) {
						console.log('error ', error);
						return;
					}
				});
			} catch (e) {
				console.log('Exception ', e);
			}
		}, _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
	}

	return MainController;
}(_base.BaseController);

exports.default = new MainController();