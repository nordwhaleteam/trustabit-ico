'use strict';

Object.defineProperty(exports, "__esModule", {
	value: true
});

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _base = require('./base.controller');

var _user = require('../models/user');

var _user2 = _interopRequireDefault(_user);

var _LoginResponse = require('../responses/auth/LoginResponse');

var _LoginResponse2 = _interopRequireDefault(_LoginResponse);

var _index = require('sequelize/lib/errors/index');

var sequelizeError = _interopRequireWildcard(_index);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var MESSAGE_WRONG_LOGIN = 'Login field cannot be blank';
var MESSAGE_WRONG_PASSWORD = 'Password field cannot be blank';
var MESSAGE_WRONG_CREDENTIALS = 'Login and password mismatch our records.';
var MESSAGE_BLOCKED_ATTEMPT = 'Login attempt failed. Please try again in 20 minutes.';

var AuthController = function (_BaseController) {
	(0, _inherits3.default)(AuthController, _BaseController);

	function AuthController() {
		var _ref,
		    _this2 = this;

		var _temp, _this, _ret;

		(0, _classCallCheck3.default)(this, AuthController);

		for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
			args[_key] = arguments[_key];
		}

		return _ret = (_temp = (_this = (0, _possibleConstructorReturn3.default)(this, (_ref = AuthController.__proto__ || Object.getPrototypeOf(AuthController)).call.apply(_ref, [this].concat(args))), _this), _this.login = function () {
			var _ref2 = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(req, res, next) {
				var _req$body, email, password, result, err, user;

				return _regenerator2.default.wrap(function _callee$(_context) {
					while (1) {
						switch (_context.prev = _context.next) {
							case 0:
								_req$body = req.body, email = _req$body.email, password = _req$body.password;
								result = void 0;
								err = {};
								_context.prev = 3;

								if (!email) {
									Object.assign(err, { 'email': MESSAGE_WRONG_LOGIN });
								}
								if (!password) {
									Object.assign(err, { 'password': MESSAGE_WRONG_PASSWORD });
								}

								if (!(Object.keys(err).length > 0)) {
									_context.next = 8;
									break;
								}

								return _context.abrupt('return', (0, _LoginResponse2.default)({ err: err, res: res }));

							case 8:
								_context.next = 10;
								return _user2.default.findOne({ where: { email: email } });

							case 10:
								user = _context.sent;

								if (!user) {
									_context.next = 36;
									break;
								}

								_context.next = 14;
								return user.loginIsAllowed();

							case 14:
								if (!_context.sent) {
									_context.next = 33;
									break;
								}

								_context.next = 17;
								return user.authenticate(password);

							case 17:
								if (!_context.sent) {
									_context.next = 30;
									break;
								}

								_context.next = 20;
								return user.generateToken();

							case 20:
								_context.t0 = _LoginResponse2.default;
								_context.next = 23;
								return user.toJSON();

							case 23:
								_context.t1 = _context.sent;
								_context.t2 = {
									user: _context.t1
								};
								_context.t3 = res;
								_context.t4 = {
									result: _context.t2,
									res: _context.t3
								};
								result = (0, _context.t0)(_context.t4);
								_context.next = 31;
								break;

							case 30:
								result = (0, _LoginResponse2.default)({ err: MESSAGE_WRONG_CREDENTIALS, res: res });

							case 31:
								_context.next = 34;
								break;

							case 33:
								result = (0, _LoginResponse2.default)({ err: MESSAGE_BLOCKED_ATTEMPT, res: res });

							case 34:
								_context.next = 37;
								break;

							case 36:
								result = (0, _LoginResponse2.default)({ err: MESSAGE_WRONG_CREDENTIALS, res: res });

							case 37:
								_context.next = 43;
								break;

							case 39:
								_context.prev = 39;
								_context.t5 = _context['catch'](3);

								console.log(_context.t5);

								result = (0, _LoginResponse2.default)({ err: _context.t5, res: res });

							case 43:
								return _context.abrupt('return', result);

							case 44:
							case 'end':
								return _context.stop();
						}
					}
				}, _callee, _this2, [[3, 39]]);
			}));

			return function (_x, _x2, _x3) {
				return _ref2.apply(this, arguments);
			};
		}(), _temp), (0, _possibleConstructorReturn3.default)(_this, _ret);
	}

	return AuthController;
}(_base.BaseController);

exports.default = new AuthController();