import Sequelize from 'sequelize';
import sequelize from '../database';

export default sequelize.define('contactus', {
	id: {
		type:Sequelize.INTEGER,
		primaryKey:true,
		autoIncrement: true
	},
	name: {
		type: Sequelize.TEXT,
		validate: {
			len: {
				args: [1, 255],
				msg: "Wrong name length",
			}
		}
	},
	email: {
		type: Sequelize.STRING,
		validate: {
			isEmail: {
				msg: "Sorry, wrong email format"
			}
		}
	},
	message: {
		type:Sequelize.TEXT,
		validate: {
			notEmpty: {
				msg: "Sorry, this field is required"
			}
		}
	},
	createdAt: Sequelize.DATE
},{
	hooks: {
		beforeCreate: (record) => {
			record.createdAt = new Date();
		}
	}
});
