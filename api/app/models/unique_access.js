import Sequelize from 'sequelize';
import sequelize from '../database';

export default sequelize.define('unique_access', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    user_id: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    ip: {
        type: Sequelize.STRING
    },
    createdAt: Sequelize.DATE
}, {
    hooks: {
        beforeCreate: (record) => {
            record.createdAt = new Date();
        }
    }
});
