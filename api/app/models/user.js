import Sequelize from 'sequelize';
import sequelize from '../database';
import bcrypt from 'bcryptjs';
import crypto from 'crypto';
import moment from 'moment';
import web3 from 'web3';

const USER_ACTIVE_STATUS_DISABLED = false;
const USER_ACTIVE_STATUS_ENABLED = true;

const MAX_LOGIN_ATTEMPTS = 10;
const WAIT_TIME_BETWEEN_ATTEMPTS = 20; //minutes


const User = sequelize.define('users', {
	id: {
		type: Sequelize.INTEGER,
		primaryKey: true,
		autoIncrement: true
	},
	email: {
		type: Sequelize.STRING,
		validate: {
			isEmail: {
				msg: "Sorry, wrong email format"
			},
			unique: async email => {
				await User.find({where: {email: email}}).then((user) => {
					if (user) {
						throw new Error('This email already in use');
					}
				});
			},
		},
		defaultValue: ''
	},
	full_name: {
		type: Sequelize.STRING,
		validate: {
			notEmpty: true,
		},
		defaultValue: ''
	},
	password: {
		type: Sequelize.TEXT('tiny'),
		validate: {
			len: {
				args: [6, 255],
				msg: "Min password length 5 characters",
			}
		},
		defaultValue: ''
	},
	wallet: {
		type: Sequelize.STRING,
                validate: {
			isValid: async wallet => {
                            if (!web3.utils.isAddress(wallet)) {
                                throw new Error('This wallet is not valid');
                            }
			},
			unique: async wallet => {
				await User.find({where: {wallet: wallet}}).then((user) => {
					if (user) {
						throw new Error('This wallet already in use');
					}
				});
			},
		},
	},
	token: {
		type: Sequelize.STRING,
	},
	attempts: {
		type: Sequelize.INTEGER,
	},
	active: {
		type: Sequelize.INTEGER(1),
		defaultValue: false
	},
	createdAt: Sequelize.DATE,
	updatedAt: Sequelize.DATE,
	lastAttemptAt: Sequelize.DATE,
	loggedInAt: Sequelize.DATE,
        referral: {
		type: Sequelize.INTEGER,
                defaultValue: null
	},
}, {
	tableName: 'users',
	hooks: {
		beforeCreate: (user) => {
			user.password = bcrypt.hashSync(user.password);
			user.createdAt = new Date();
		},
		beforeUpdate: async function (user) {
			if (!bcrypt.compareSync(user.password, user.previous('password')) && user.password !== user.previous('password')) {
				user.password = bcrypt.hashSync(user.password);
			}
			user.updatedAt = new Date();
		},
	}
});

User.prototype.authenticate = async function (password) {
	console.log(bcrypt.compareSync(password, this.password));
	const result = bcrypt.compareSync(password, this.password);
	if (result) {
		await this.updateAttributes({
			attempts: 0,
			loggedInAt: new Date()
		});
	} else await this.trackAttempt();
	return result;
};

User.prototype.loginIsAllowed = async function () {
	let result = true;

	let diff = moment().diff(moment(this.lastAttemptAt, 'YYYY-MM-DD HH:mm:ss'), 'minutes');
	if (this.attempts >= MAX_LOGIN_ATTEMPTS && WAIT_TIME_BETWEEN_ATTEMPTS >= diff) {
		await this.trackAttempt();
		result = false;
	}

	return result;
};

User.prototype.trackAttempt = async function () {
	await this.updateAttributes({
		attempts: ++this.attempts,
		lastAttemptAt: new Date()
	});
};

User.prototype.generateToken = async function () {
	await this.updateAttributes({token: await crypto.randomBytes(32).toString('hex')});
	return this.token;
};

User.prototype.toJSON = async function () {
	return {
		id: this.id,
		email: this.email,
		token: this.token,
		active: this.active,
		wallet: this.wallet,
		full_name: this.full_name
	};
};

export default User;