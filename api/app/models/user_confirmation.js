import Sequelize from 'sequelize';
import sequelize from '../database';
import User from '../models/user';

const CONFIRMATION_ACTION_REGISTER = 1;
const CONFIRMATION_ACTION_RESET = 2;

const UserConfirmation = sequelize.define('users_confirmations', {
	id: {
		primaryKey: true,
		type: Sequelize.UUID,
		defaultValue: Sequelize.UUIDV4,
		validate: {
			isUUID: 4
		},
	},
	user_id: {
		type: Sequelize.INTEGER,
	},
	code: {
		type: Sequelize.TEXT,
	},
	type: {
		type: Sequelize.INTEGER
	},
	used: {
		type: Sequelize.BOOLEAN,
		defaultValue: false
	},
	createdAt: {
		type: Sequelize.DATE
	},
	expireAt: {
		type: Sequelize.DATE
	},
}, {
	tableName: 'users_confirmations',
	hooks: {
		beforeCreate: (userConfirmation) => {
			userConfirmation.createdAt = new Date();
			if(userConfirmation.type === CONFIRMATION_ACTION_RESET){
				userConfirmation.expireAt = new Date(new Date() + (24 * 60 * 60 * 1000));
			}
		},
	}
});

UserConfirmation.belongsTo(User, {foreignKey: 'user_id', targetKey: 'id'});

export {UserConfirmation, CONFIRMATION_ACTION_REGISTER, CONFIRMATION_ACTION_RESET};