import Constants from '../../config/constants';

export const tokenContractAddressLocal = '0xdc676542a8ffaa062880a3eac080bf3c79a303aa';
export const tokenContractAddressProd = '0x4ddaf983302f451a5a6dc95af102335bf280d9a7';
export const tokenContractAddressRinkeby = '0x539912ebdfa78fba72d5e962ec458e1e600f105d';

export const icoAddressLocal = '0xb499cee2ddf86e43d438c4267f67cd03df832dbe';
export const icoAddressProd = '0x39Db2AB26F405916f0020ca12bAA95e56fADF599';
export const icoAddressRinkeby = '0xd58c92411afddb98a88666b4f14ddea7e8f30e7f';

export const providerLocal = 'http://localhost:8545';
export const providerProd = 'https://mainnet.infura.io';
export const providerRinkeby = 'https://rinkeby.infura.io';

export const providerScanLocal = 'http://localhost:8545';
export const providerScanProd = 'https://mainnet.etherscan.io/tx';
export const providerScanRinkeby = 'https://rinkeby.etherscan.io/tx';


export const tokenContractAddress = Constants.envs.test ? tokenContractAddressLocal : (Constants.envs.production ? tokenContractAddressProd : tokenContractAddressRinkeby);
export const icoAddress = Constants.envs.test ? icoAddressLocal : (Constants.envs.production ? icoAddressProd : icoAddressRinkeby);
export const provider = Constants.envs.test ? providerLocal : (Constants.envs.production ? providerProd : providerRinkeby);
export const providerScan = Constants.envs.test ? providerScanLocal : (Constants.envs.production ? providerScanProd : providerScanRinkeby);