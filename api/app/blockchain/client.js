import Tx from 'ethereumjs-tx';
import Web3 from 'web3';

const config = {
  web3: {
    gasValue: 4000000,
    waitDelay: 1000,
  },
};

export let web3 = new Web3();
export const GAS_VALUE = config.web3.gasValue;

export const getProvider = (provider) => new Web3.providers.HttpProvider(
  provider);
export const setProvider = async (provider) => await web3.setProvider(
  getProvider(provider));

export const getNonce = async (from) => {
  const trxCount = await web3.eth.getTransactionCount(from);
  console.log('transactions ', trxCount);
  return trxCount;
};

export const getContract = (abi, address) => new web3.eth.Contract(abi, address);

export const gasPrice = () => new Promise((resolve, reject) => web3.eth.getGasPrice((err, gasPrice) => {
    if (err) {
      return reject(err);
    }
    return resolve(gasPrice.toString(10));
  }),
);

export const waitTx = (hash) => new Promise((resolve, reject) => {
  console.log('waiting... ' + hash);
  const timer = setInterval(() => {
    web3.eth.getTransaction(hash, async (err, tx) => {
      if (err) {
        return reject(err);
      }
      if (tx && tx.blockNumber && tx.blockHash) {
        clearInterval(timer);
        const receipt = await web3.eth.getTransactionReceipt(hash);
        return resolve(receipt);
      }
    });
  }, config.web3.waitDelay);
});

export const call = async (contractMethod, ...props) => {
  const result = await contractMethod.call(...props);
  try {
    return JSON.parse(result);
  } catch (e) {
    return result;
  }
};

export const prepareTx = async (contractMethod, from, gas, ...props) => {
  props.push({from, gas});
  const tx = new Tx(contractMethod.request.apply(this, props).params[0]);
  tx.nonce = await getNonce(from);
  tx.gasPrice = gasPrice();
  tx.gas = gas;
  return tx;
};

export const sendEther = async (wallet, gas, data) => {
  const from = wallet.getAddressString();
  const tx = new Tx({
    from,
    to: data.to,
    nonce: await getNonce(from),
    gasPrice: gasPrice(),
    value: data.value,
    gas,
  });
  console.log('nonce ', tx.nonce);
  tx.sign(wallet.getPrivateKey());
  const serializedTx = tx.serialize();
  let hexTx = toHex(serializedTx.toString('hex'));
  return await sendRaw(hexTx);
};

export const sendEtherSync = async (wallet, to, value) => {
  const data = {
    to: toHex(to),
    value: web3.toWei(parseInt(value * 100, 10), 'ether') / 100,
  };
  return await sendEther(wallet, GAS_VALUE, data);
};

export const sendRaw = async (tx) => new Promise((resolve, reject) => {
  web3.eth.sendRawTransaction(tx, (err, txHash) => {
    if (err) {
      console.log('sendRaw', err);
      return reject(err);
    }
    return resolve(txHash);
  });
});

export const sendTx = async (wallet, contractMethod, gas, ...props) => {
  const address = wallet.getAddressString();
  const tx = await prepareTx(contractMethod, address, gas, ...props);
  tx.sign(wallet.getPrivateKey());
  const serializedTx = tx.serialize();
  let hexTx = toHex(serializedTx.toString('hex'));
  return await sendRaw(hexTx);
};

export const toHex = (value) => {
  return `${value}`.startsWith('0x') ? value : '0x' + value;
};

export const getTransactionUrl = (hash) => `${config.web3.httpProviderScan}/${hash}`;
