import { Router } from 'express';

import MainController from './controllers/main.controller';
import AuthController from './controllers/auth.controller';

import errorHandler from './middleware/errorHandler';
import accessControl from './middleware/access-control';

const routes = new Router();

routes.get('/', MainController.index);
routes.get('/user', accessControl, MainController.getUser);

routes.get('/unique_acess/:id', MainController.saveUniqueAccess);
routes.get('/refferal_info/:id', MainController.getReffrealInfo);

routes.post('/main/contactus', MainController.contactus);
routes.get('/main/confirm', MainController.confirm);
routes.post('/main/subscribe', MainController.subscribe);
routes.post('/main/reset', MainController.reset);
routes.post('/main/register', MainController.register);
routes.post('/auth/login', AuthController.login);
routes.post('/setWallet', accessControl, MainController.setWallet);

routes.use(errorHandler);

export default routes;
