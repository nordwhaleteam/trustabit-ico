import UniqueAccess from '../models/unique_access'

export const createUniqueAccess = async (params) => {
    console.log(params);
    UniqueAccess.findOrCreate({
        where: {
            ip: params.ip
        },
        defaults: {
            user_id: params.user_id
        }
    }).then(function (unique_access) {
        return true;
    });
};

export const getUniqueAccessCount = async (user) => {
    console.log(user);
    const result = await UniqueAccess.findAndCountAll({
		where: {user_id: user.id}
	});
    return result;
};

export default {
    createUniqueAccess,
    getUniqueAccessCount
};
