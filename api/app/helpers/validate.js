import Joi from "joi";
import ReCaptcha from "recaptcha2";
import appConfig from '../config/constants';

export const validateRecaptcha = async (req, res, next) => {
    let err = {};
    
    if (req.body['g-recaptcha-response'] === undefined || req.body['g-recaptcha-response'] === '' || req.body['g-recaptcha-response'] === null) {
        Object.assign(err, {'recaptcha': 'Captcha is required'});
    } 
    
    try {
        const result = await recaptcha(req.body['g-recaptcha-response']);
        if (!result) {
            Object.assign(err, {'recaptcha': 'Captcha you entered is wrong or expired'});
        }
    } catch (e) {
        Object.assign(err, {'recaptcha': 'Captcha you entered is wrong or expired'});
    }
    
    return err;
}

const recaptcha = async (key) => {
  const r = new ReCaptcha({
    siteKey: appConfig.google.recaptcha.publicKey,
    secretKey: appConfig.google.recaptcha.privateKey
  });
  return new Promise((resolve, reject) => { 
    r.validate(key).then(resolve).catch(reject);
  })
};