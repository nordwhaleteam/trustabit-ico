import dotenv from 'dotenv';

const env = dotenv.config().parsed;
const blank = 'check_config';

export default {
	DB_HOST: env.DB_HOST || blank,
	DB_NAME: env.DB_NAME || blank,
	DB_USER: env.DB_USER || blank,
	DB_PASS: env.DB_PASS || blank,
	DB_TYPE: env.DB_TYPE || blank,

	MAIL_HOST: env.MAIL_HOST || blank,
	MAIL_PORT: env.MAIL_PORT || blank,
	MAIL_LOGIN: env.MAIL_LOGIN || blank,
	MAIL_PASS: env.MAIL_PASS || blank,
	MAIL_CONTACT: env.MAIL_CONTACT || blank,

	HOST_NAME: env.HOST_NAME || blank,
};