import UserBonuses from '../models/user_bonuses'

export const createBonus = async (params) => {
    console.log(params);
    UserBonuses.findOrCreate({
        where: {
            from_user: params.from_user,
            to_user: params.to_user
        },
        defaults: {
            amount: params.amount
        }
    }).then(function (user_bonuses) {
        if(user_bonuses[0]) {
            user_bonuses[0].update({amount:params.amount});
        }
        return true;
    });
};

export const deleteAll = async () => {
    return UserBonuses.destroy({ truncate : true, cascade: false })
};

export const getBonusSum = async (user) => {
    console.log(user);
    const result = await UserBonuses.sum('amount', {
		where: {to_user: user.id}
	});
    return result;
};

export default {
    createBonus,
    deleteAll,
    getBonusSum
};
