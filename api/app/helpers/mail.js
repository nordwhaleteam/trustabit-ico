import fs from 'fs';
import Path from 'path';
import Handlebars from 'handlebars';
import NodeMailer from 'nodemailer';
import mailConfig from '../config/mail';

export default (transport) => {
  if (!transport) {
    transport = mailConfig.transport;
  }
  const transporter = NodeMailer.createTransport(transport);

  const sendContactUsMail = (data) => {
    const templatePath = Path.resolve(__dirname, '../views/emails',
        'contact.html');
    const template = Handlebars.compile(fs.readFileSync(templatePath, 'utf8'));
    const subject = `New request: ${data.name}`;
    const html = template(data);
    const sendMailData = Object.assign({}, mailConfig.contact, {subject, html});

    return transporter.sendMail(sendMailData, (err, info) => {
      console.log(err);
    });
  };

  const sendRegistrationEmail = (data) => {
    const templatePath = Path.resolve(__dirname, '../views/emails',
        'register.html');
    const template = Handlebars.compile(fs.readFileSync(templatePath, 'utf8'));
    const subject = `Registration on TrustaBit`;
    const html = template(data);

    const sendMailData = Object.assign({}, mailConfig.contact,
        {subject, html, to: data.email});

    return transporter.sendMail(sendMailData, (err, info) => {
      console.log(err);
    });
  };

  const sendResetPasswordEmail = (data) => {
    const templatePath = Path.resolve(__dirname, '../views/emails',
        'reset.html');
    const template = Handlebars.compile(fs.readFileSync(templatePath, 'utf8'));
    const subject = `TrustaBit password reset`;
    const html = template(data);

    const sendMailData = Object.assign({}, mailConfig.contact,
        {subject, html, to: data.email});

    transporter.sendMail(sendMailData, (err, info) => {
      console.log(err);
    });
  };

  const sendSubscribeEmail = (data) => {
    try {
      const templatePath = Path.resolve(__dirname, '../views/emails',
          'subscribe.html');
      const template = Handlebars.compile(
          fs.readFileSync(templatePath, 'utf8'));
      const subject = `TrustaBit. Subscription`;
      const html = template(data);

      const sendMailData = Object.assign({}, mailConfig.contact,
          {subject, html, to: data.email});

      return transporter.sendMail(sendMailData, (err, info) => {
        console.log(err);
      });
    } catch (e) {
      console.log(e);
    }
  };

  return {
    sendContactUsMail,
    sendRegistrationEmail,
    sendResetPasswordEmail,
    sendSubscribeEmail,
  };
}
