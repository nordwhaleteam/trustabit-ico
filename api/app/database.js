import Sequelize from 'sequelize';
import config from './helpers/config';

export default new Sequelize(config.DB_NAME, config.DB_USER, config.DB_PASS, {
	host: config.DB_HOST,
	dialect: config.DB_TYPE,
	define: {
		timestamps: false // true by default
	},
	pool: {
		max: 5,
		min: 0,
		idle: 10000
	},
});

