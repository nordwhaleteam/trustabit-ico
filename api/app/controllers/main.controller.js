import {BaseController} from './base.controller';
import Contactus from '../models/contactus';
import errorHandler from '../middleware/errorHandler';
import response from '../middleware/response';
import mailing from '../helpers/mail';
import UserHelper from "../helpers/user";
import UserBonusHelper from "../helpers/user_bonuses";
import UniqueAccessHelper from "../helpers/unique_access";
import Sequelize from 'sequelize';
import confirmResponse from '../responses/main/confirmResponse';
import RegisterResponse from '../responses/auth/RegisterResponse';
import request from 'request';
import appConfig from '../config/constants';
import {validateRecaptcha} from '../helpers/validate';

class MainController extends BaseController {

	contactUsWhiteList = [
		'name',
		'email',
		'message'
	];

	registerWhiteList = [
		'full_name',
		'email',
		'password',
		'password_repeat',
                'referral',
                'wallet'
	];

	updateWhiteList = [
		'wallet'
	];

	contactus = async (req, res, next) => {
		try {
			const params = this.filterParams(req.body, this.contactUsWhiteList);
			let model = new Contactus({
				...params
			});
			const result = await model.save();
			//this.addToMailchimp(req.body.email,req.body.name);
			const mail = await mailing().sendContactUsMail(params);
			return response(req, res, result);
		} catch (err) {
			return errorHandler(err, req, res, next);
		}
	};

	index = async (req, res) => {
		res.json({
			version: '0.0.1',
		});
	};

	getUser = async (req, res, next) => {
		try {
			return res.json(await req.currentUser.toJSON());
		} catch (err) {
			errorHandler(err, req, res, next);
		}
	};
        
        saveUniqueAccess = async (req, res, next) => {
		try {
                    let ip = req.connection.remoteAddress;
                    let user_id = req.params.id;
		    let result = await UniqueAccessHelper.createUniqueAccess({ip, user_id});
                    return res.status(200).json({result:result});
		} catch (err) {
		    errorHandler(err, req, res, next);
		}
	};
        
        getReffrealInfo = async (req, res, next) => {
		try {
                    let unique_access = await UniqueAccessHelper.getUniqueAccessCount({id:req.params.id});
                    let bonus = await UserBonusHelper.getBonusSum({id:req.params.id});
                    let refferals = await UserHelper.findReferrals({id:req.params.id});
                    
                    return res.status(200).json({
                        unique_access : unique_access.count,
                        bonus : bonus,
                        refferals_count : refferals.count
                    });
		} catch (err) {
                    errorHandler(err, req, res, next);
		}
	};

	register = async (req, res, next) => {
                let validationErrors = await validateRecaptcha(req, res, next);
                
                if (Object.keys(validationErrors).length > 0) {
                    return res.status(422).json({errors: validationErrors});
                }
                
		try {
                    const params = this.filterParams(req.body, this.registerWhiteList);
                    const user = await UserHelper.createUser({...params});
                    res.json({user});
		} catch (err) {
                    errorHandler(err, req, res, next);
		}
	};
	
	setWallet = async (req, res, next) => {
		try {
			const params = this.filterParams(req.body, this.updateWhiteList);
			res.json(await UserHelper.updateUser(req.currentUser, {wallet:params.wallet}));
		} catch (err) {
			errorHandler(err, req, res, next);
		}
	};


	subscribe = async (req, res, next) => {
		try {
			let model = new Contactus({
				email: req.body.email
			}, {skip: ['name', 'message']});
			await model.save();

			//this.addToMailchimp(req.body.email,'');

			/*if (appConfig.env === 'production') {
				this.sendGForm(req.body);
			} else {
				this.sendGFormTest(req.body);
			}*/

			mailing().sendSubscribeEmail(req.body);
			return response(req, res, {});
		} catch (err) {
			return errorHandler(err, req, res, next);
		}
	};

	reset = async (req, res, next) => {
		try {
			const id = req.param('id');
			const code = req.param('code');
			const password = req.body.password;
			const password_repeat = req.body.password_repeat;
			if (id && code) { /*reset confirm*/
				// if (password) {
				const userModel = await UserHelper.confirmUserPasswordReset({id, code, password, password_repeat});
				return RegisterResponse({ result: { user: await userModel.toJSON() }, res });
			} else { /*reset request*/
				await UserHelper.requestUserPasswordReset({email: req.body.email});
			}
			return response(req, res, {});
		} catch (err) {
			return errorHandler(err, req, res, next);
		}
	};

	confirm = async (req, res, next) => {
		let response = null;
		try {
			await UserHelper.confirmUserRegistration({
				id: req.param('id')
			});
			response = confirmResponse({req, res, next});
		} catch (err) {
			response = confirmResponse({err, req, res, next});
		}
		return response;
	};

	sendGForm = (req) => {
		try {
			// request.post('https://docs.google.com/forms/d/e/1FAIpQLScjf-sJzxgIJ_yCFnRQMeL5EIkHwhgMREaZdvLMSTwV1fEYOw/formResponse', {
			// 	form: {
			// 		'entry.403294950': req.email ? req.email : '',
			// 		'entry.770328930': req.utm_source ? req.utm_source : '',
			// 		'entry.1668652964': req.utm_medium ? req.utm_medium : '',
			// 		'entry.355906419': req.utm_campaign ? req.utm_campaign : '',
			// 		'entry.760235735': req.utm_content ? req.utm_content : '',
			// 		'entry.1395438923': req.utm_term ? req.utm_term : '',
			// 		'entry.1062312962': req.reflink ? req.reflink : ''
			// 	}
			// });
		} catch (e) {
			console.log(e);
		}
	};

	sendGFormTest = (req) => {
		try {
			//old test google form (modultraid)
			request.post('https://docs.google.com/forms/d/e/1FAIpQLSetgWw-jdtcnPt6327tV14-mrnzGEiW47fN4iUtcJPhSeiKXA/formResponse', {
				form: {
					'entry.282765473': req.email ? req.email : '',
					'entry.1916199099': req.utm_source ? req.utm_source : '',
					'entry.1600229878': req.utm_medium ? req.utm_medium : '',
					'entry.595357331': req.utm_campaign ? req.utm_campaign : '',
					'entry.1447209678': req.utm_content ? req.utm_content : '',
					'entry.1560038263': req.utm_term ? req.utm_term : '',
					'entry.1560038263': req.reflink ? req.reflink : ''
				}
			});
		} catch (e) {
			console.log(e);
		}
	};

	addToMailchimp = (email, name) => {
		try {
			let dataCenter = appConfig.mailChimp.apiKey.split('-')[1];
			request({
				url: `https://${dataCenter}.api.mailchimp.com/3.0/lists/${appConfig.mailChimp.listId}/members`,
				json: {
					'email_address': email,
					'status': 'subscribed',
					'merge_fields': {
						'FNAME': name,
					}
				},
				method: 'POST',
				headers: {
					'Content-Type': 'application/json',
					'Authorization': `apikey ${appConfig.mailChimp.apiKey}`
				}
			}, function (error, response, body) {
				if (error) {
					console.log('error ',error);
					return;
				}
			});
		} catch (e) {
			console.log('Exception ',e);
		}
	}

}

export default new MainController();
