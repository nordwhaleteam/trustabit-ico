import {BaseController}  from './base.controller';
import User from '../models/user';
import LoginResponse from '../responses/auth/LoginResponse';
import * as sequelizeError from "sequelize/lib/errors/index";
const MESSAGE_WRONG_LOGIN = 'Login field cannot be blank';
const MESSAGE_WRONG_PASSWORD = 'Password field cannot be blank';
const MESSAGE_WRONG_CREDENTIALS = 'Login and password mismatch our records.';
const MESSAGE_BLOCKED_ATTEMPT = 'Login attempt failed. Please try again in 20 minutes.';

class AuthController extends BaseController {
	login = async (req, res, next) => {
		const {email, password} = req.body;
		let result;
		let err = {};
		try {
			if (!email) {
				Object.assign(err, { 'email': MESSAGE_WRONG_LOGIN });
			}
			if (!password) {
				Object.assign(err, {'password': MESSAGE_WRONG_PASSWORD});
			}
			if (Object.keys(err).length > 0) {
				return LoginResponse({ err: err, res });

			}
			const user = await User.findOne({where: {email}});
			
			if (user) {
				if (await user.loginIsAllowed()) {
					if (await user.authenticate(password)) {
						await user.generateToken();
						result = LoginResponse({ result: {user: await user.toJSON()}, res});
					} else {
						result = LoginResponse({err: MESSAGE_WRONG_CREDENTIALS, res});
					}
				} else {
					result = LoginResponse({err: MESSAGE_BLOCKED_ATTEMPT, res});
				}
			} else {
				result = LoginResponse({err: MESSAGE_WRONG_CREDENTIALS, res});
			}
		} catch (err) {
			console.log(err);
			
			result = LoginResponse({err: err, res});
		}
		return result;
	};
}

export default new AuthController();
