import jwt from 'jsonwebtoken';
import User from '../models/user';
import Constants from '../config/constants';

const { sessionSecret } = Constants.security;

export default async function authenticate(req, res, next) {
  const { authorization } = req.headers;
  try {
    const user = await User.findOne({
      where: { token: `${authorization}`.substr(7) }
    });
    if (!user) {
      return res.sendStatus(401);
    }
    req.currentUser = user;
    next();
  } catch(err) {
    console.log(err)
    next(err);
  }
  // jwt.verify(authorization, sessionSecret, async (err, decoded) => {
  //   console.log(err, decoded);
    
  //   if (err) {
  //     return res.sendStatus(401);
  //   }

  //   // If token is decoded successfully, find user and attach to our request
  //   // for use in our route or other middleware
  //   try {
  //     const user = await User.findById(decoded._id);
  //     if (!user) {
  //       return res.sendStatus(401);
  //     }
  //     req.currentUser = user;
  //     next();
  //   } catch(err) {
  //     next(err);
  //   }
  // });
}
