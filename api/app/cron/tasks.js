import logger from '../helpers/logger';
import UserHelper from "../helpers/user";
import UserBonusHelper from "../helpers/user_bonuses";
import erc20 from '../blockchain/contracts/erc20';
import web3 from 'web3';

const percentage = require('percentage-calc');

export const calculateBonus = async () => {
  try {
    let users = await UserHelper.findAll();
    for (let i = 0, len = users.length; i < len; i++) {
      let ref_first = await UserHelper.findReferrals(users[i]);

      if (ref_first.count > 0) {
        for (let j = 0, len_first = ref_first.rows.length; j < len_first; j++) {
          try{

            await saveBonus({
              to_user: users[i],
              from_user: ref_first.rows[j].dataValues,
              percent: 5
            });

          }catch(e){
            console.log(e)
          }

          let ref_second = await UserHelper.findReferrals(ref_first.rows[j].dataValues);

          if (ref_second.count > 0) {
            for (let k = 0, len_second = ref_second.rows.length; k < len_second; k++) {
              try{

                await saveBonus({
                  to_user: users[i],
                  from_user: ref_second.rows[k].dataValues,
                  percent: 1
                });

              }catch(e){
                console.log(e);
              }
            }

          }
        }
      }
    }
        
  } catch (e) {
    logger.error(e);
  }
  return Promise.resolve();
};

const saveBonus = async (params) => {
    let balance = await getBalance(params.from_user.wallet);
    if (balance) {
      const bonus_amount = percentage.of(params.percent, balance);
      await UserBonusHelper.createBonus({
        to_user: params.to_user.id,
        from_user: params.from_user.id,
        amount: parseInt(bonus_amount.toFixed(1), 10)
      });
    }
};

const getBalance = async (wallet) => {
    if (web3.utils.isAddress(wallet)) {
        const tokenContract = await erc20.create();
        return await tokenContract.balanceOf(wallet);
    }
    return null;
};