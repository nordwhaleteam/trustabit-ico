import * as tasks from './tasks';
import cron from 'node-cron';
import logger from "../helpers/logger";

try {
    tasks.calculateBonus();
    
    cron.schedule('* * */3 * *', async () => {
        await tasks.calculateBonus();
    }, true);

} catch (e) {
    logger.info(e);
    process.exit();
}

