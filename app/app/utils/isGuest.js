import getUser from 'app/utils/getUser';

export default () => {
  const user = getUser();
  return !((user && user.token) || undefined);
};

