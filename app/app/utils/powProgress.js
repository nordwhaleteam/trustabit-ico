/**
 * Correct Progress: quick at start - slow at finish.
 * NOTE: Newer reach 100%
 * @param {number} value - current value of progress;
 * @param {number} max - max value of progress;
 * @return {number} - percent of progress [0-100]%
 */
export default function correctProgress(value, max) {
  value = +value || 0;
  max = +max || 1;
  let x = Math.floor(value * 100 / max);
  return Math.floor((1 - Math.pow(0.96, x)) * 100);
}
