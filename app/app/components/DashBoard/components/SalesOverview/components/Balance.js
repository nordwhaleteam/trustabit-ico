import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import FlexColumn from 'common/FlexColumn';
import colors from 'style/colors';
import priceFormat from 'app/utils/priceFormat';

const Container = styled(FlexColumn)`
`;
const Title = styled.h3`
color:${colors.font};
font-size: 21px;
font-weight: 300;
`;
const Amount = styled.div`
font-size: 38px;
color: ${colors.fontPrimary};
`;

export default class Balance extends PureComponent {
  static propTypes = {
    title: PropTypes.any,
    amount: PropTypes.any,
    currency: PropTypes.string,
  };

  getAmount() {
    const {amount} = this.props;
    if (Number.isNaN(amount) || !amount) {
      return 0;
    }
    return amount;
  }

  render() {
    const {currency, title, digits = 2} = this.props;
    return (
      <Container>
        <Title>{title}</Title>
        <Amount>{priceFormat(this.getAmount(), currency, digits)}</Amount>
      </Container>
    );
  }
}
