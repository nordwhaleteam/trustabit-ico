import React, {PureComponent} from 'react';
import styled from 'styled-components';
import Paper from 'common/Paper';
import ProgressBar from 'common/ProgressBar';
import FlexColumn from 'common/FlexColumn';
import FlexBetween from 'common/FlexBetween';
import colors from 'style/colors';
import Balance from './components/Balance';
import trans from 'trans';
import {media} from 'style/containers';
import correctProgress from '../../../../utils/powProgress';

const Container = styled(Paper)`
margin-bottom: 25px;
`;
const Title = styled.h2`
color:${colors.fontSecondary};
font-weight: normal;
	font-size: 21px;
	${media.phone`
        text-align: center;
        `}
    ${media.desktop`
        text-align: left;
        `}
`;
const ProgressTitle = styled(Title)`
text-align:center;
margin:25px 0;
`;
const FlexBetweenCentered = styled(FlexBetween)`
  ${media.phone`
        flex-flow:column;
        align-items:center;
        text-align: center;
        `}
    ${media.desktop`
        align-items:left;
        flex-flow:row;
        text-align: left;
        `}
`;
export default class SalesOverview extends PureComponent {
  render() {
    const {balance, ethRate, tokenPrice, ethRaised, progress} = this.props;
    return (
      <Container>
        <FlexColumn>
          <Title>{trans('sales.overview')}</Title>
          <FlexBetweenCentered>
            <Balance title={trans('your.current.balance')}
                     amount={balance}
                     digits={0}
                     currency='TAB'
            />
            <Balance title={trans('eth.tab')}
                     amount={tokenPrice > 0 && 1 / tokenPrice}
                     digits={0}
            />
            <Balance title={trans('usd.tab')}
                     amount={tokenPrice > 0 && ethRate > 0 &&
                     ethRate * tokenPrice} currency='USD'
                     digits={5}
            />
            <Balance title={trans('costs.contributed')}
                     amount={ethRaised * ethRate}
                     currency='USD'/>
          </FlexBetweenCentered>
          <div>
            <ProgressTitle>{trans('token.sale.progress')}</ProgressTitle>
            <ProgressBar
              max={100}
              value={progress && correctProgress(progress.value, progress.max) || 0}
            />
          </div>
        </FlexColumn>
      </Container>
    );
  }
}
