import React, {PureComponent} from 'react';
import styled from 'styled-components';
import Paper from 'common/Paper';
import Flex from 'common/Flex';
import QR from './components/QR';
import UserTabs from './components/UserTabs';
import {icoAddress} from 'blockchain-config/eth';
import {media} from 'style/containers';
import FlexBetween from 'common/Flex';

const Container = styled(Paper)`
  ${media.phone`
        padding:0;
        `}
    ${media.tablet`
        padding:25px;
        `}
`;

const FlexBetweenCentered = styled(FlexBetween)`
  ${media.phone`
        flex-flow:column;
        align-items:center;
        `}
    ${media.tablet`
        flex-flow:row;
        `}
`;

export default class WalletOverview extends PureComponent {
  render() {
    return (
      <Container>
        <FlexBetweenCentered>
          <QR value={icoAddress}/>
          <UserTabs {...this.props}/>
        </FlexBetweenCentered>
      </Container>
    );
  }
}
