import React, {PureComponent} from 'react';

import styled from 'styled-components';
import DateFormat from 'common/DateFormat';
import Row from 'common/Table/Row';
import Table from 'common/Table/Table';
import Column from 'common/Table/Column';
import priceFormat from 'app/utils/priceFormat';

const DateColumn = styled(Column)`
  width:180px;
`;

const EthColumn = styled(Column)`
  width:120px;
`;
const TokenColumn = styled(Column)`
  flex-grow:1;
  text-align: right;
`;
export default class History extends PureComponent {

  render() {
    const {history} = this.props;
    return (
      <Table>
        {history && history.map((h, index) => (
          <Row key={index}>
            <DateColumn><DateFormat isFull date={h.date}/></DateColumn>
            <EthColumn>{priceFormat(h.eth,'ETH',4)} </EthColumn>
            <TokenColumn>{priceFormat(h.token,'TAB',4)}</TokenColumn>
          </Row>
        ))}
      </Table>
    );
  }
}
