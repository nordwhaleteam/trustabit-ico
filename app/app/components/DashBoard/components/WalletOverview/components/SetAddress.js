import React, {PureComponent} from 'react';
import styled from 'styled-components';
import trans from 'trans';
import colors from 'style/colors';
import FlexCenter from 'common/FlexCenter';
import TextField from 'common/TextField';
import RaisedButton from 'common/RaisedButton';

const TabContentSetAddress = styled(FlexCenter)`
min-height: 303px;
background: ${colors.lightBg};
padding:25px;
display: flex;
`;
const Label = styled.div`
text-align: center;
font-size: 14px;
color:${colors.fontDark};
`;
const Block = styled.div`
// width: 337px;
`;
const ButtonContainer = styled.div`
margin-top:10px;
width: 232px;
max-width:100%;
`;
export default class SetAddress extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {wallet: props && props.user && props.user.wallet || ''};
  }

  componentWillReceiveProps(nextProps) {
    const {wallet} = this.state;
    this.setState({
      wallet: wallet || nextProps && nextProps.user && nextProps.user.wallet || '',
    });
  }

  handleChangeAddress = (e, wallet) => {
    this.setState({wallet});
  };
  handleSetAddress = () => {
    const {wallet} = this.state;
    const {onSetAddress} = this.props;
    onSetAddress({wallet});
  };

  render() {
    const {errorWallet, loading} = this.props;
    const {wallet} = this.state;
    return (
      <TabContentSetAddress>
        <Block>
          <Label>
            {trans('enter.address.label')}
          </Label>
          <TextField fullWidth value={wallet}
                     errorText={errorWallet && trans('error.address')}
                     disabled={loading}
                     onChange={this.handleChangeAddress}
                     floatingLabelText={trans('enter.address')}/>
          <FlexCenter>
            <ButtonContainer>
              <RaisedButton primary fullWidth label={trans('proceed')}
                            disabled={!wallet || loading}
                            onClick={this.handleSetAddress}/>
            </ButtonContainer>
          </FlexCenter>
        </Block>
      </TabContentSetAddress>
    );
  }
}
