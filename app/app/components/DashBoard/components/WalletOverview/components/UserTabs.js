import React, {PureComponent} from 'react';
import styled from 'styled-components';
import {Tab, Tabs} from 'material-ui/Tabs';
import trans from 'trans';
import colors from 'style/colors';
import SetAddress from './SetAddress';
import History from './History';
import {media} from 'style/containers';

const styles = {
  tabItemContainerStyle: {
    backgroundColor: colors.white,
  },
  inkBarStyle: {
    backgroundColor: colors.brandInfo,
  },
  tab: {
    color: colors.fontDark,
  },
  historyTab: {
    color: colors.fontDark,
  },
  tabs: {
    height: '100%',
  },
};
const Container = styled.div`
padding:0 25px;
flex-grow: 1;
    ${media.phone`
        padding: 0;
        width: 100%;
        `}
    ${media.tablet`
        padding:0 25px;
        width: auto;
        `}
`;

const TabContent = styled.div`
min-height: 303px;
background: ${colors.lightBg};
padding:25px;
overflow-x:scroll;
`;

export default class UserTabs extends PureComponent {
  handleGetHistory = () => {
    const {user, onGetHistory} = this.props;
    onGetHistory(user);
  };

  render() {
    return (
      <Container>
        <Tabs style={styles.tabs} inkBarStyle={styles.inkBarStyle}
              tabItemContainerStyle={styles.tabItemContainerStyle}>
          <Tab label={trans('wallet')} style={styles.tab}>
            <SetAddress {...this.props}/>
          </Tab>
          <Tab label={trans('history')} style={styles.historyTab}
               onActive={this.handleGetHistory}>
            <TabContent>
              <History {...this.props}/>
            </TabContent>
          </Tab>
        </Tabs>
      </Container>
    );
  }
}
