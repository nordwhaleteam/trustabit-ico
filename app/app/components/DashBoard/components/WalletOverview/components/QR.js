import React, {PureComponent} from 'react';
import styled from 'styled-components';
import Paper from 'material-ui/Paper';
import QRCode from 'qrcode.react';
import FlexCenter from 'common/FlexCenter';
import trans from 'trans';
import colors from 'style/colors';
import IcoContract from 'app/blockchain/contracts/ico';
import {tokenContractAddress} from 'blockchain-config/eth';
import {media} from 'style/containers';

const Container = styled(Paper)`
    padding: 0.5em;
    margin-bottom:1em;
display: flex;
align-items: center;
justify-content: center;
width:100%;
${media.phone`
        flex: 0 auto;
        padding-bottom:1em;
        min-height: auto;
        `}
    ${media.tablet`
        flex: 0 0.5 50%;
        
        `}
`;
const Address = styled.div`
    font-weight: bold;
    font-size: 14px;
    text-align: center;
    margin-top: 8px;
    word-break: break-all;

`;
const AddressTitle = styled.div`
    font-weight: bold;
    font-size: 1.2em;
    text-align: center;
    margin-top: 8px;
    margin-bottom: 10px;
    color: #21ad8d;
`;

const AddressComment = styled.div`
    font-size: 1em;
    text-align: center;
    margin-top: 8px;
    margin-bottom: 10px;
`;
const Label = styled.div`
    font-size: 14px;
    text-align: center;
    margin-top: 8px;
    padding: 0 5px;
    > span {
      color:${colors.brandDanger};
   }
`;
const SuggestedRow = styled.div`
  display: flex;
  flex-flow: row;
  justify-content: center;
`;
const ArticleLink = styled.div`
    font-size: 14px;
    text-align: center;
    margin-top: 8px;
`;
export default class QR extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      gasPrice: '',
    };
  }

  componentWillMount() {
    this.getGasPrice().then((gasPrice) => {
      this.setState({gasPrice:Math.floor(gasPrice/1000000000)});
    });
  }

  async getGasPrice() {
    const contract = await IcoContract.instance();
    return await contract.getGasPrice();
  }

  render() {
    const {value} = this.props;
    const {gasPrice} = this.state;
    return (
      <Container>
        <div>
          <AddressTitle>{trans('transfer.title')}</AddressTitle>
          <AddressComment>{trans('transfer.comment')}</AddressComment>
          <FlexCenter>
            <QRCode value={value} size={200}/>
          </FlexCenter>
          <Address>{value}</Address>
          <SuggestedRow>
            <Label>{trans('gas.price')}</Label>
            <Label>{gasPrice} GWei</Label>
          </SuggestedRow>
          <SuggestedRow>
            <Label>{trans('gas.limit')}</Label>
            <Label> 400 000</Label>
          </SuggestedRow>
          <Label>{trans('copy.address')}</Label>
          <Label><span>{trans('dashboard.warning')}</span></Label>
          <ArticleLink><a
            href='https://medium.com/@TrustaBit/how-to-contribute-to-the-trustabit-token-sale-bbdefae5c3f6'
            target="_blank">{trans('article.link')}</a></ArticleLink>
          <Label>{trans('token.address')}: {tokenContractAddress}</Label>
        </div>
      </Container>
    );
  }
}
