import React from 'react';
import styled from 'styled-components';
import colors from 'style/colors';

export default styled.h3`
color:${colors.white};
font-size: 21px;
text-align: center;
font-weight: 300;
margin-bottom: 25px;
`;
