import React, {PureComponent} from 'react';
import moment from 'moment';
import styled from 'styled-components';
import colors from 'style/colors';
import trans from 'trans';
import FlexColumn from 'common/FlexColumn';
import FlexCenter from 'common/FlexCenter';
import graphScheme from 'common/img/Scheme.png';
import graphWallet from 'common/img/wallet.png';
import graphGroup from 'common/img/Group.png';
import graphDeal from 'common/img/deal.png';
import Title from './Title';
import {CopyToClipboard} from 'react-copy-to-clipboard';

const ReferralBlockCover = styled.div`
  background: ${colors.lightGreyBlue};
  margin: 0.5em;
`;
const ReferralFlexBlock = styled.div`
  background: ${colors.white};
  color: ${colors.font};
  margin: 1em;
  box-shadow: 0px 5px 2px 0 rgba(119,151,178,0.48);
`;
const ReferralTitle = styled(Title)`
  margin-top: 25px;
`;
const ReferralColumn = styled.div`
  padding: 1em;
  display-flex:flex;
  flex-flow: column wrap;
  justify-content: center;
  border-bottom: 1px solid ${colors.lightGrey};
`;
const ColumnTitle = styled.p`
  font-size: 21px;
  text-align: center;
`;
const Text = styled.p`
  font-size: 2em;
  color: ${colors.fontSecondary};
  text-align: center;
`;
const GraphBlock = styled(FlexCenter)`
  padding: 0.5em 0;
`;
const GraphScheme = styled.img`
  width: 100%;
  max-width: 200px;
`;
const IconGraph = styled.img`
  width: 100%;
  max-width: 25px;
  margin-right: 10px;
`;
const ReferralLink = styled.div`
  color: #21ad8d;
  text-decoration: underline;
  text-align: center;
  cursor: pointer;
  font-size: 1.2em;
  font-weight:bold;
  padding: 0.5em 0;
 `;
export default class ReferralBlock extends PureComponent {
  render() {
    const {user, referral_info} = this.props;
    const referral_url = 'https://' + window.location.hostname + '/index.html?referral=' + user.id;
    return (
      <ReferralBlockCover>
        <ReferralTitle>{trans('referral.block.title')}</ReferralTitle>
        <ReferralFlexBlock>
          <ReferralColumn>
            <ColumnTitle>{trans('referral.block.bonus.balance')}</ColumnTitle>
            <Text><IconGraph src={graphWallet}/>{referral_info && referral_info.bonus ? referral_info.bonus : '0'} {trans('currency.TAB')}</Text>
          </ReferralColumn>
          <ReferralColumn>
            <ColumnTitle>{trans('referral.block.unique.access')}</ColumnTitle>
            <Text><IconGraph src={graphGroup}/>{referral_info && referral_info.unique_access ? referral_info.unique_access : '0'}</Text>
          </ReferralColumn>
          <ReferralColumn>
            <ColumnTitle>{trans('referral.block.invited.friends')}</ColumnTitle>
            <Text><IconGraph src={graphDeal}/>{referral_info && referral_info.refferals_count ? referral_info.refferals_count : '0'}</Text>
          </ReferralColumn>
          <ReferralColumn>
            <GraphBlock>
              <GraphScheme src={graphScheme}/>
            </GraphBlock>
          </ReferralColumn>
          <ReferralColumn>
          <CopyToClipboard text={referral_url}>
            <ReferralLink>{trans('referral.block.copy.link')}</ReferralLink>
          </CopyToClipboard>
          </ReferralColumn>
        </ReferralFlexBlock>
      </ReferralBlockCover>
    );
  }
}
