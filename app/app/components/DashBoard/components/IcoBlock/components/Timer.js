import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import FlexColumn from 'common/FlexColumn';
import FlexCenter from 'common/FlexCenter';
import Title from './Title';
import trans from 'trans';
import FlexBetween from 'common/FlexBetween';
import TimeBlock from './TimeBlock';

const Container = styled(FlexColumn)`
margin: 40px 0 40px 0;
`;
const TimerContainer = styled(FlexBetween)`
width: 266px;
`;

export default class Timer extends PureComponent {
  static propTypes = {
    from: PropTypes.any,
    to: PropTypes.any,
  };

  constructor(props) {
    super(props);
    this.state = {...this.getTimerObject()};
  }

  componentWillMount() {
    this.interval = setInterval(() => {
      this.setState({...this.getTimerObject()});
    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  getTimerObject() {
    let {to, onEnd, type} = this.props;
    if (type === 'end' || type === 'loading') {
      return;
    }
    const total = Date.parse(new Date(+to)) - Date.parse(new Date());
    const obj = {
      total,
      days: Math.floor(total / (1000 * 60 * 60 * 24)),
      hours: Math.floor((total / (1000 * 60 * 60)) % 24),
      minutes: Math.floor((total / 1000 / 60) % 60),
      seconds: Math.floor((total / 1000) % 60),
    };

    if (total <= 0) {
      if (typeof onEnd === 'function') {
        onEnd();
      }
    } else {
      return {
        days: `00${obj.days}`.substr(-2),
        hours: `00${obj.hours}`.substr(-2),
        minutes: `00${obj.minutes}`.substr(-2),
        seconds: `00${obj.seconds}`.substr(-2),
      };
    }
  }

  render() {
    const {type} = this.props;
    const {days = '', hours = '', minutes = '', seconds = ''} = this.state;
    return (
      <Container>
        <Title>{trans(`ico.starts.${type}`)}</Title>
        {type !== 'end' && (
          <FlexCenter>
            <TimerContainer>
              <TimeBlock value={days} label={trans('days')}/>
              <TimeBlock value={hours} label={trans('hours')}/>
              <TimeBlock value={minutes} label={trans('minutes')}/>
              <TimeBlock value={seconds} label={trans('seconds')}/>
            </TimerContainer>
          </FlexCenter>
        )}
      </Container>
    );
  }
}
