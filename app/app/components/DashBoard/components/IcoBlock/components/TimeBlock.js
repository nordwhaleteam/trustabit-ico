import React, {PureComponent} from 'react';
import styled from 'styled-components';
import colors from 'style/colors';
import FlexColumn from 'common/FlexColumn';

const Container = styled(FlexColumn)`
`;
const Box = styled.div`
    background: #2d303d;
    border-radius: 4px;
    width: 44px;
    height: 58px;
    text-align: center;
    color: #fff;
    font-size: 20px;
    line-height: 58px;
`;
const Label = styled.div`
    color: ${colors.white};
    text-transform: uppercase;
    font-size: 10px;
    text-align: center;
`;
export default class Timer extends PureComponent {
  render() {
    const {value,label} = this.props
    return (
      <Container>
          <Box>{value}</Box>
          <Label>{label}</Label>
      </Container>
    );
  }
}
