import React, {PureComponent} from 'react';
import moment from 'moment';
import styled from 'styled-components';
import colors from 'style/colors';
import FlexColumn from 'common/FlexColumn';
import FlexCenter from 'common/FlexCenter';
import graphDist from 'common/img/distribution.png';
import graphAllocate from 'common/img/allocation.png';
import trans from 'trans';
import Timer from './components/Timer';
import Title from './components/Title';
import {mainSaleEndDate, mainSaleStartDate, preSaleEndDate, preSaleStartDate,} from 'blockchain-config/eth';
import {media} from 'style/containers';
import ReferralBlock from './components/ReferralBlock';

const Container = styled(FlexColumn)`

background: ${colors.darkGreyBlue};
flex: 0 0.22 22%;
padding:1em;
  ${media.phone`
        flex: 0 0 auto;
        `}
    ${media.desktop`
        flex: 0 0.22 22%;
        `}
`;
export default class IcoBlock extends PureComponent {
  getToTimeType() {
    const {
      preSaleStartDate,
      preSaleEndDate,
      mainSaleStartDate,
      mainSaleEndDate,
    } = this.props;
    if (!preSaleStartDate ||
      !preSaleEndDate ||
      !mainSaleStartDate ||
      !mainSaleEndDate) {
      return 'loading';
    }
    if (moment() < moment(preSaleStartDate)) {
      return 'preSaleStartDate';
    }
    if (moment() < moment(preSaleEndDate)) {
      return 'preSaleEndDate';
    }
    if (moment() < moment(mainSaleStartDate)) {
      return 'mainSaleStartDate';
    }
    if (moment() < moment(mainSaleEndDate)) {
      return 'mainSaleEndDate';
    }
    return 'end';
  }

  handleTimerEnd = () => {
    this.forceUpdate();
    setTimeout(() => this.forceUpdate(), 1000);
  };

  render() {
    const toDateType = this.getToTimeType();
    return (
      <Container>
        <Timer to={this.props[toDateType]} type={toDateType} onEnd={this.handleTimerEnd}/>
        <ReferralBlock {...this.props} />
      </Container>
    );
  }
}
