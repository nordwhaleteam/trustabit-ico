import React, {PureComponent} from 'react';
import styled from 'styled-components';
import FlexColumn from 'common/FlexColumn';
import SalesOverview from '../SalesOverview';
import WalletOverview from '../WalletOverview';
import colors from 'style/colors';
import {media} from 'style/containers';


const Container = styled(FlexColumn)`
background: ${colors.lightGreyBlue};
flex: 0 0.78 78%;
padding:25px;
${media.phone`
        flex: 0;
        flex: 0 0 auto;
        `}
    ${media.desktop`
        flex: 0 0.78 78%;
        `}
`;

export default class Overview extends PureComponent {
  render() {
    return (
      <Container>
        <SalesOverview {...this.props}/>
        <WalletOverview {...this.props}/>
      </Container>
    );
  }
}
