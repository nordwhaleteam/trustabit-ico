import React, {PureComponent} from 'react';
import Flex from 'common/Flex';
import styled from 'styled-components';
import IcoBlock from './components/IcoBlock';
import Overview from './components/Overview';
import {media} from 'style/containers';


const Container = styled(Flex)`
    display:flex;
    ${media.phone`
        flex-flow:column-reverse;
        `}
    ${media.desktop`
        flex-flow:row;
        `}
`;


export default class DashBoard extends PureComponent {
  render() {
    return (
      <Container>
        <IcoBlock {...this.props}/>
        <Overview {...this.props}/>
      </Container>
    );
  }
}
