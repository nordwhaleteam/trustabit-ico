import React, {PureComponent} from 'react';
import PropTypes from 'prop-types';
import {assign} from 'lodash';
import colors from 'app/colors';
import IWarning from 'material-ui/svg-icons/alert/warning';
import ICheck from 'material-ui/svg-icons/action/check-circle';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import styled from 'styled-components';
import Flex from 'modultrade/app/app/components/common/Flex';
import trans from 'app/trans';

const IconWarning = styled(IWarning)`
  fill:${colors.brandWarning}!important;
  min-width: 50px!important;
  height: 50px!important;
  margin: 0 24px 0 0;
`;

const IconCheck = styled(ICheck)`
  fill:${colors.brandSuccess}!important;
  min-width: 50px!important;
  height: 50px!important;
  margin: 0 24px 0 0;
`;

export default class ModalConfirmation extends PureComponent {
  static propTypes = {
    open: PropTypes.bool,
    withIcon: PropTypes.string,
    onSubmit: PropTypes.func,
    onClose: PropTypes.func,
    labelCancel: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    labelSubmit: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
    contentStyle: PropTypes.object,
    actions: PropTypes.array,
  };

  static defaultProps = {
    labelCancel: trans('no'),
    labelSubmit: trans('yes'),
    children: trans('confirmMessage'),
  };

  constructor(props, context) {
    super(props, context);
    this.state = {
      open: false,
    };
    this.contentStyle = assign({}, props.contentStyle, {
      width: 500,
    });

    this.defaultActions = [
      <FlatButton
        label={props.labelCancel}
        primary
        onClick={this.handleClose}
      />,
      <FlatButton
        label={props.labelSubmit}
        primary
        keyboardFocused
        onClick={this.handleSubmit}
      />,
    ];
  }

  componentWillReceiveProps(props) {
    this.setState({
      open: (props && props.open) || false,
    });
  }

  handleClose = () => {
    const {onClose} = this.props;
    if (typeof onClose === 'function') {
      onClose();
    }
    this.setState({open: false});
  };
  handleSubmit = () => {
    this.handleClose();
    const {onSubmit} = this.props;
    if (typeof onSubmit === 'function') {
      onSubmit();
    }
  };

  render() {
    const {actions, withIcon, labelCancel, labelSubmit, contentStyle, ...props} = this.props; // eslint-disable-line no-unused-vars
    return this.state.open ? (
      <Dialog
        contentStyle={this.contentStyle}
        actions={actions || this.defaultActions} modal={false}
        open={this.state.open} onRequestClose={this.handleClose} {...props}
      >
        {withIcon && (<div>
          <Flex>
            {withIcon === 'check' && (
              <IconCheck/>
            )}
            {withIcon === 'warning' && (
              <IconWarning/>
            )}
            <div>{this.props.children}</div>
          </Flex>
        </div>)}
        {!withIcon && this.props.children}
      </Dialog>
    ) : null;
  }
}
