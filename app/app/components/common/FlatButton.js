import React from 'react'
import styled from 'styled-components';
import colors from 'style/colors';
import fonts from 'style/font-styles';
import FlatButton from 'material-ui/FlatButton';

const Container = styled(FlatButton)`
height:auto!important;
border-radius: 1em!important;
line-height: 1!important;
font-size:${fonts.smallSize};
padding:0.7em 0;
`;
export default class extends React.PureComponent {
  render() {
    return <Container {...this.props}/>
  }
}
