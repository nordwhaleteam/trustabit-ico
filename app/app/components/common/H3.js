import styled from 'styled-components';

export default styled.h3`
  font-size: 16px;
  margin: 0;
`;
