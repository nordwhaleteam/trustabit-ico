import React, {PureComponent} from 'react'; // eslint-disable-line no-unused-vars
import PropTypes from 'prop-types'; // eslint-disable-line no-unused-vars
import moment from 'moment';

export const serverFormat = (date) => moment(date).format('YYYY-MM-DD 00:00:00');
export const dateFormat = (date) => moment(date).format('DD/MM/YYYY');
export const dateFullFormat = (date) => moment(date).
  format('DD/MM/YYYY H:mm:ss');
export const dateFormatShort = (date) => moment(date).format('DD/MM');
export const dateFormatShortYear = (date) => moment(date).format('DD/MM/YY');

class DateFormat extends PureComponent {
  static propTypes = {
    date: PropTypes.any,
    isFull: PropTypes.bool,
    isShort: PropTypes.bool,
    isShortYear: PropTypes.bool,
  };

  render() {
    let {date, isFull, isShort, isShortYear, ...props} = this.props;

    if (isShort) {
      return <span {...props}>{dateFormatShort(date)}</span>;
    }
    if (isShortYear) {
      return <span {...props}>{dateFormatShortYear(date)}</span>;
    }
    if (isFull) {
      return <span {...props}>{dateFullFormat(date)}</span>;
    }
    return <span {...props}>{dateFormat(date)}</span>;
  }
}

export default DateFormat;
