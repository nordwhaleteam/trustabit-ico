import SelectField from 'material-ui/SelectField';
import styled from 'styled-components';

export default styled(SelectField)`
  label{
    text-transform: uppercase!important;
  }

`