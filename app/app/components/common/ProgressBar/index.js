import React from 'react';
import LinearProgress from 'material-ui/LinearProgress';
import colors from 'style/colors';
import styled from 'styled-components';

const Progress = styled(LinearProgress)`
background: ${colors.lightGrey}!important;
height: 16px!important;
    border-radius: 8px!important;
`;

export default class ProgressBar extends React.PureComponent {
  render() {
    return (
      <Progress color={colors.brandInfo} mode="determinate" {...this.props}/>
    );
  }
}