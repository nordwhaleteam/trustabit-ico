import styled from 'styled-components';
import React from 'react';
import PropTypes from 'prop-types';
import colors from 'style/colors';

const Container = styled.div`
  color:${colors.fontPrimary};
  > div:nth-child(odd){
    background:${colors.lightBg};
  }
  
  > div:nth-child(even){
    background:#fff;
  }

`;
export default class Table extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    children: PropTypes.any,
  };

  render() {
    const { theme } = this.context;
    return (
      <Container>
        {this.props.children}
      </Container>
    );
  }
}
