import styled from 'styled-components';
import colors from 'style/colors';

export default styled.div`
  background: ${colors.white};
  padding: 25px;
  box-shadow: 0px 5px 2px 0 rgba(119, 151, 178, 0.48)
`;