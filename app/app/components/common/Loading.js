import React from 'react';
import CircularProgress from 'material-ui/CircularProgress';
import FlexCenter from 'common/FlexCenter';
import styled from 'styled-components';

const Container = styled(FlexCenter)`
min-height: 300px;
`;

export default () => (
  <Container>
    <div><CircularProgress size={80} thickness={5} /></div>
  </Container>
);