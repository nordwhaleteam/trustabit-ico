import React from 'react';
import PropTypes from 'prop-types';
import Paper from 'material-ui/Paper';
import styled from 'styled-components';

const Container = styled(Paper)`
  background-color: ;
  border: 1px solid #dedede;
   
    padding:20px;
  width:100%;
`;
export default class Panel extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    children: PropTypes.any,
  };

  render() {
    return (
      <Container {...this.props} zDepth={3}>
        {this.props.children}
      </Container>
    );
  }
}
