import React from 'react';
import styled from 'styled-components';
import RaisedButtonMaterial from 'material-ui/RaisedButton';

const RaisedButton = styled(RaisedButtonMaterial)`
      border-radius: 20px!important;
      * {
          border-radius: 20px!important;
      }
`;
export default class extends React.PureComponent {
  render() {
    return (
      <RaisedButton {...this.props}/>
    );
  }
}
