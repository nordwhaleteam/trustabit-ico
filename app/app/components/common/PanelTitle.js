import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import colors from 'app/colors';

const Container = styled.h1`
    font-size: 22px;
    text-align: center;
    color: ${colors.title};
`;
export default class Panel extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    children: PropTypes.any,
  };

  render() {
    return (
      <Container {...this.props}>
        {this.props.children}
      </Container>
    );
  }
}
