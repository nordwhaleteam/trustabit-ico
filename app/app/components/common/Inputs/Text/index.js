import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';


const Container = styled.div`
  display:inline-block;
`;
const Error = styled.div`
  color:red;
`;
const Label = styled.div`
  color:blue;
`;

export default class Text extends PureComponent {
  static propTypes = {
    type: PropTypes.any,
    error: PropTypes.any,
    label: PropTypes.any,
    value: PropTypes.any,
    onChange: PropTypes.func,
    multiLine: PropTypes.bool,
    placeholder: PropTypes.any,
    rows: PropTypes.any
  };
  static defaultProps = {
    type: 'text',
    multiLine: false,
    onChange: () => {
    }
  };

  constructor(props) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    const { onChange } = this.props
    onChange(e.target.value, e)
  }

  render() {
    const { onChange, value, label, type, multiLine, error, ...props } = this.props;
    const Component = multiLine ? 'textarea' : 'input';
    if (!multiLine) {
      props.type = type;
    }
    return <Container>
      {label && (
        <Label>{label}</Label>
      )}
      <Component onChange={this.handleChange} value={value} {...props}/>
      {error && (
        <Error>{error}</Error>
      )}
    </Container>

  }


}
