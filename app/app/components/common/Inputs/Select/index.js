import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';
import Row from './components/Row';
import styled from 'styled-components';


const Container = styled.div`
  display:inline-block;
`;
const Error = styled.div`
  color:red;
`;
const Label = styled.div`
  color:blue;
`;

export default class Select extends PureComponent {
  static propTypes = {
    value: PropTypes.any,
    label: PropTypes.any,
    error: PropTypes.any,
    onChange: PropTypes.func,
    list: PropTypes.any,
    defaultValue: PropTypes.any,
    valueField: PropTypes.string,
    labelField: PropTypes.string
  };
  static defaultProps = {
    onChange: () => {
    },
    list: [],
    valueField: 'id',
    labelField: 'name',
    defaultValue: ''

  };

  constructor(props) {
    super(props);
    this.state = {
      expanded: false
    };
    this.handleToggle = this.handleToggle.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleToggle() {
    const { expanded } = this.state;
    this.setState({ expanded: !expanded })
  }

  handleChange(value, item, event) {
    const { onChange } = this.props;
    const { expanded } = this.state;
    onChange(value, item, event);
    this.setState({ expanded: !expanded })
  }

  getLabel() {
    const { list, valueField,labelField, value } = this.props;
    if (list && list.length > 0) {
      const item = list.find((i) => i[valueField] === value);
      return item && item[labelField];
    }
    return ''
  }

  render() {
    const { expanded } = this.state;

    const { onChange, label, error, value, defaultValue, labelField, valueField, list } = this.props;
    return (
      <Container>
        {label && (
          <Label>{label}</Label>
        )}
        <button type="button" onClick={this.handleToggle}>{this.getLabel() || defaultValue}</button>
        {expanded && (
          <ul>
            {list && list.map((item, index) => (
              <Row
                key={index}
                onChange={this.handleChange} item={item} valueField={valueField} labelField={labelField}
                selected={value === item && item[valueField]}
              />
            ))}
          </ul>
        )}
        {label && (
          <Error>{error}</Error>
        )}
      </Container>
    );
  }
}
