import PropTypes from 'prop-types';
import React, { PureComponent } from 'react';

export default class Row extends PureComponent {

  static propTypes = {
    onChange: PropTypes.func,
    item: PropTypes.object,
    valueField: PropTypes.string,
    labelField: PropTypes.string,
  };

  static defaultProps = {
    onChange: () => {
    },
    item: {}
  };

  constructor(props) {
    super(props);
    this.state = {};
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    const { onChange, item, valueField } = this.props;
    onChange(item[valueField], item, e)
  }

  render() {
    const { item, labelField } = this.props;
    return (
      <li>
        <a onClick={this.handleChange} href='javascript:void(0)'>{item && item[labelField]}</a>
      </li>
    );
  }
}
