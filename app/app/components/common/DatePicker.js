import DatePicker from 'material-ui/DatePicker';
import styled from 'styled-components';

export default styled(DatePicker)`
  label{
    text-transform: uppercase!important;
  }

`