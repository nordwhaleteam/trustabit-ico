import React from 'react';
import FlexCenter from 'common/FlexCenter';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import trans from 'trans';
import logOut from 'utils/logOut';

/**
 * Simple Icon Menus demonstrating some of the layouts possible using the `anchorOrigin` and
 * `targetOrigin` properties.
 */
export default class UserMenu extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {open: false};
  }

  handleOpen = () => {
    this.setState({open: true});
  };
  onRequestChange = (open) => {
    this.setState({open});
  };

  render() {
    const {user} = this.props;
    const {open} = this.state;
    return (
      <FlexCenter>
        <FlatButton label={user && user.full_name|| 'User'} onClick={this.handleOpen}/>
        <IconMenu
          open={open}
          onRequestChange={this.onRequestChange}
          onClick={this.handleOpen}
          iconButtonElement={<IconButton><MoreVertIcon/></IconButton>}
          anchorOrigin={{horizontal: 'left', vertical: 'top'}}
          targetOrigin={{horizontal: 'left', vertical: 'top'}}
        >
          <MenuItem primaryText={trans('logout')} onClick={logOut}/>
        </IconMenu>
      </FlexCenter>
    );
  }
}
