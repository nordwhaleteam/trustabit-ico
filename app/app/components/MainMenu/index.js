import React, {PureComponent} from 'react';
import styled from 'styled-components';
import colors from 'style/colors';
import logoImg from 'common/img/logo.png';
import FlexBetween from 'common/FlexBetween';
import UserMenu from './components/UserMenu';

const Container = styled(FlexBetween)`

width:100%;
background: ${colors.white};
box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.5);
z-index: 1;
`;
const Logo = styled.img`
      height: 60px;
    width: 115px;
    display: block;
    padding: 0.5em;
    margin: 5px 0 0 38px;
`;
export default class MainMenu extends PureComponent {
  render() {
    return (
      <Container>
        <a href="/"><Logo src={logoImg}/></a>
        <UserMenu {...this.props}/>
      </Container>
    );
  }
}
