import styled from 'styled-components';
import { css } from 'styled-components';


export const sizes = {
  large: 1920,
  big: 1200,
  desktop: 991,
  tablet: 767,
  phone: 177
}

// iterate through the sizes and create a media template
export const media = Object.keys(sizes).reduce((accumulator, label) => {
  // use em in breakpoints to work properly cross-browser and support users
  // changing their browsers font-size: https://zellwk.com/blog/media-query-units/
  const emSize = sizes[label]
  accumulator[label] = (...args) => css`
    @media (min-width: ${emSize}px) {
      ${css(...args)}
    }
    
  `
  return accumulator
}, {})

export const Container = styled.div`
    margin-right: auto;
    margin-left: auto;
    padding-left: 20px;
    padding-right: 20px;
    max-width: 100%;
    display:block;
    &:after, &:before  {
        content: " ";
        display: table;
    }
    &:after {
        clear: both;
    }
    ${media.tablet`
        width: 760px;
        max-width: 100%;`}
    ${media.desktop` 
        width: 980px;
        max-width: 100%;`}
   
    ${media.big`
        width: 1180px;
        max-width: 1180px;`}
  
    
`;

export const ContainerFluid = styled.div`
    margin-right: auto;
    margin-left: auto;
    padding-left: 20px;
    padding-right: 20px;
    &:after, &:before  {
        content: " ";
        display: table;
    }
    &:after {
        clear: both;
    }
    ${media.phone`
        padding:0;`}
    ${media.desktop`
        padding-left: 20px;
        padding-right: 20px;`}
    
`;



export const TitleSection = styled.div`
    display:flex;
    flex-flow:row wrap;
    justify-content:space-between;
    align-items: center;
    ${media.phone`
        flex-flow:column wrap;
        align-items:center;
        margin: 0 0 2.5em;
        >h2 {
        text-align:center;
        margin:1.5em 0;
        }
        `}
    ${media.tablet`
        flex-flow:row wrap;
        align-items:center;
        margin: 0 0 0;
        >h2 {
        text-align:left;
        
        }
        `}
    
`;
