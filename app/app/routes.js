import {getAsyncInjectors} from 'utils/asyncInjectors';
import isGuest from 'utils/isGuest';
import logOut from 'utils/logOut';

const errorLoading = (err) => {
  console.error('Dynamic page loading failed', err); // eslint-disable-line no-console
};

const loadModule = (cb) => (componentModule) => {
  cb(null, componentModule.default);
};

export default function createRoutes(store) {
  // Create reusable async injectors using getAsyncInjectors factory
  const {injectReducer, injectSagas} = getAsyncInjectors(store); // eslint-disable-line no-unused-vars

  return [
    {
      path: '*',
      name: 'home',
      onEnter() {
        if (isGuest()) {
          logOut();
        }
      },
      getComponent(nextState, cb) {
        const importModules = Promise.all([
          import('containers/DashBoardContainer'),
          import('containers/DashBoardContainer/reducer'),
        ]);

        const renderRoute = loadModule(cb);

        importModules.then(([component, reducer]) => {
          renderRoute(component);
          injectReducer('dashBoardContainer', reducer.default);
        });

        importModules.catch(errorLoading);
      },
    },
  ];
}
