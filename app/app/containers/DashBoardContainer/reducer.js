import typeToReducer from 'type-to-reducer';
import {fromJS} from 'immutable';
import {
  GET_BALANCE, 
  GET_CURRENT_PRICE, 
  GET_DATES, 
  GET_ETH_RAISED, 
  GET_ETH_RATE, 
  GET_HISTORY, 
  GET_PROGRESS, 
  GET_USER,
  SET_WALLET,
  GET_REFERRAL_INFO
} from './constants';
import reducerParse from 'app/utils/reducerParse';
import setUser from 'app/utils/setUser';
import {mainSaleEndDate, mainSaleStartDate, preSaleEndDate, preSaleStartDate} from 'blockchain-config/eth';

const initialState = fromJS({});

export default typeToReducer({
  [GET_USER]: {
    SUCCESS: (state = fromJS([]), d) => reducerParse(d, (data) => {
        return state;
      }
      ,
      (payload) => {
        const {data, status} = payload;
        return state;
      },
    ),
  },
  [GET_ETH_RAISED]: {
    SUCCESS: (state = fromJS([]), d) => {
      return state.set('ethRaised', d && d.payload);
    },
  },
  [GET_BALANCE]: {
    START: (state) => state.set('errorWallet', null),
    SUCCESS: (state = fromJS([]), d) => {
      return state.set('balance', d && d.payload);
    },
    FAIL: (state, d) => {
      return state.set('errorWallet', fromJS(d.payload.message));
    },
  },
  [GET_CURRENT_PRICE]: {
    SUCCESS: (state = fromJS([]), d) => {
      return state.set('tokenPrice', d && d.payload);
    },
  },
  [GET_DATES]: {
    SUCCESS: (state = fromJS([]), {payload}) => {
      return state.set('preSaleStartDate', fromJS(payload && payload.preSaleStartDate)).
        set('preSaleEndDate', fromJS(payload && payload.preSaleEndDate)).
        set('mainSaleStartDate', fromJS(payload && payload.mainSaleStartDate)).
        set('mainSaleEndDate', fromJS(payload && payload.mainSaleEndDate));
    },
  },
  [GET_PROGRESS]: {
    SUCCESS: (state = fromJS([]), d) => {
      return state.set('progress', d && d.payload);
    },
  },
  [GET_CURRENT_PRICE]: {
    SUCCESS: (state = fromJS([]), d) => {
      return state.set('tokenPrice', d && d.payload);
    },
  },
  [SET_WALLET]: {
    START: (state) => {
      return state.set('loading', true);
    },
    FAIL: (state) => {
      return state.set('loading', false);
    },
    SUCCESS: (state = fromJS([]), d) => reducerParse(d, (data) => {
        setUser(data);
        return state.set('user', data).set('loading', false);
      }
      ,
      (payload) => {
        const {data, status} = payload;
        return state;
      },
    ),
  },
  [GET_HISTORY]: {
    START: (state) => {
      return state;
    },
    FAIL: (state) => {
      return state;
    },
    SUCCESS: (state = fromJS([]), d) => {
      return state.set('history', d.payload);
    },
  },
  [GET_REFERRAL_INFO]: {
    START: (state) => {
      return state;
    },
    FAIL: (state) => {
      return state;
    },
    SUCCESS: (state = fromJS([]), d) => reducerParse(d, (data) => {
        return state.set('referral_info', data);
      }
      ,
      (payload) => {
        const {data, status} = payload;
        return state;
      },
    ),
  },
  [GET_ETH_RATE]: {
    SUCCESS: (state = fromJS([]), d) => reducerParse(d, (data) => {
        if (data[0] && data[0].price_usd) {
          return state.set('ethRate', fromJS(data[0] && data[0].price_usd));
        }
        return state;
      }
      ,
      (payload) => {
        const {data, status} = payload;
        return state;
      },
    ),
  },
}, initialState);
