import React from 'react';
import immutableProps from 'app/hocs/immutableProps';
import {connect} from 'react-redux';
import DashBoard from 'app/components/DashBoard';
import {setAddressStory} from './story';
import {
  getBalanceAction, 
  getCurrentPriceAction, 
  getDatesAction, 
  getEthRaisedAction, 
  getEthRateAction, 
  getHistoryAction,
  getProgressAction, 
  getUserAction,
  getReferralInfoAction
} from './actions';

class DashBoardContainer extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function

  loadData() {
    const {
        onGetEthRate, 
        onGetHistory, 
        onGetEthRaised, 
        onGetProgress, 
        getCurrentPrice, 
        user, 
        onGetBalance,
        onGetReferralInfo
    } = this.props;
    
    onGetEthRate();
    onGetEthRaised();
    onGetProgress();
    getCurrentPrice();
    if (user) {
      onGetReferralInfo(user);
      if (user.wallet) {
        onGetBalance(user);
        onGetHistory(user);
      }
    }
  }

  componentWillMount() {
    const {onGetUser, onGetDates} = this.props;
    onGetUser();
    onGetDates();
    this.loadData();
    setInterval(() => {
      this.loadData();
    }, 10 * 1000);
  }

  render() {
    return (
      <DashBoard {...this.props}/>
    );
  }
}

const mapStateToProps = (state) => ({
  balance: state.getIn(['dashBoardContainer', 'balance']),
  referral_info: state.getIn(['dashBoardContainer', 'referral_info']),
  ethRate: state.getIn(['dashBoardContainer', 'ethRate']),
  tokenPrice: state.getIn(['dashBoardContainer', 'tokenPrice']),
  ethRaised: state.getIn(['dashBoardContainer', 'ethRaised']),
  errorWallet: state.getIn(['dashBoardContainer', 'errorWallet']),
  loading: state.getIn(['dashBoardContainer', 'loading']),
  progress: state.getIn(['dashBoardContainer', 'progress']),
  history: state.getIn(['dashBoardContainer', 'history']),
  preSaleStartDate: state.getIn(['dashBoardContainer', 'preSaleStartDate']),
  preSaleEndDate: state.getIn(['dashBoardContainer', 'preSaleEndDate']),
  mainSaleStartDate: state.getIn(['dashBoardContainer', 'mainSaleStartDate']),
  mainSaleEndDate: state.getIn(['dashBoardContainer', 'mainSaleEndDate']),
  user: state.getIn(['app', 'user']),
});
const mapDispatchToProps = (dispatch) => ({
  onSetAddress: (data) => setAddressStory(dispatch, data),
  onGetBalance: (data) => dispatch(getBalanceAction(data)),
  onGetUser: (data) => dispatch(getUserAction(data)),
  getCurrentPrice: (data) => dispatch(getCurrentPriceAction(data)),
  onGetEthRate: (data) => dispatch(getEthRateAction(data)),
  onGetEthRaised: (data) => dispatch(getEthRaisedAction(data)),
  onGetProgress: (data) => dispatch(getProgressAction(data)),
  onGetHistory: (data) => dispatch(getHistoryAction(data)),
  onGetDates: (data) => dispatch(getDatesAction(data)),
  onGetReferralInfo: (data) => dispatch(getReferralInfoAction(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(
  immutableProps(DashBoardContainer));
