export const SET_BALANCE = 'app/DashBoardContainer/SET_BALANCE';
export const GET_BALANCE = 'app/DashBoardContainer/GET_BALANCE';
export const SET_WALLET = 'app/DashBoardContainer/SET_WALLET';
export const GET_ETH_RATE = 'app/DashBoardContainer/GET_ETH_RATE';
export const GET_USER = 'app/DashBoardContainer/GET_USER';
export const GET_CURRENT_PRICE = 'app/DashBoardContainer/GET_CURRENT_PRICE';
export const GET_ETH_RAISED = 'app/DashBoardContainer/GET_ETH_RAISED';
export const GET_PROGRESS = 'app/DashBoardContainer/GET_PROGRESS';
export const GET_HISTORY = 'app/DashBoardContainer/GET_HISTORY';
export const GET_DATES = 'app/DashBoardContainer/GET_DATES';
export const GET_REFERRAL_INFO = 'app/DashBoardContainer/GET_REFERRAL_INFO';