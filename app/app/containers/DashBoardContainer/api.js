import {apiCreate} from 'app/global-constants';
import {create} from 'apisauce';
import erc20 from '../../blockchain/contracts/erc20';
import ico from '../../blockchain/contracts/ico';

export const getUser = () => apiCreate().get('/user');
export const setUserWallet = (data) => apiCreate().post('/setWallet', data);
export const getBalance = async ({wallet}) => {
  const tokenContract = await erc20.create();
  return await tokenContract.balanceOf(wallet);
};

export const getCurrentPrice = async () => {
  try {
    const contract = await ico.create();
    return await contract.getCurrentPrice();
  } catch (e) {
    return e;
    console.log(e);
  }
};
export const getEthRaised = async () => {
  try {
    const contract = await ico.create();
    return await contract.getEthRaised();
  } catch (e) {
    console.log(e);
  }
};

export const getProgress = async () => {
  try {
    const contract = await ico.create();
    const max = +await contract.getAvailableToken();
    let value = +await contract.getTokenRaised();
    return {max, value};
  } catch (e) {
    console.log(e);
  }
};

export const getDates = async () => {
  const contract = await ico.create();
  const [
    preSaleStartDate,
    preSaleEndDate,
    mainSaleStartDate,
    mainSaleEndDate,
  ] = await Promise.all([
    contract.preSaleStartDate(), contract.preSaleEndDate(), contract.mainSaleStartDate(), contract.mainSaleEndDate(),
  ]);
  return {
    preSaleStartDate,
    preSaleEndDate,
    mainSaleStartDate,
    mainSaleEndDate,
  };
};

export const getHistory = async ({wallet}) => {
  const contract = await ico.instance();
  return await contract.getHistory({investor: wallet});
};

export const getEthRate = () => create({
  baseURL: 'https://api.coinmarketcap.com',
}).get('/v1/ticker/ethereum/');

export const getReferralInfo = (data) => apiCreate().get('/refferal_info/' + data.id);