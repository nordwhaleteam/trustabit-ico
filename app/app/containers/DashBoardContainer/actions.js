import {createAction} from 'redux-actions';
import {
  GET_BALANCE, GET_CURRENT_PRICE, GET_ETH_RAISED, GET_ETH_RATE, GET_PROGRESS,
  GET_USER, SET_WALLET, GET_HISTORY, GET_DATES, GET_REFERRAL_INFO
} from './constants';
import {
  getBalance, getCurrentPrice, getEthRaised, getEthRate, getProgress, getUser,
  setUserWallet, getHistory, getDates, getReferralInfo
} from './api';

export const getBalanceAction = createAction(GET_BALANCE,
  async (d) => await getBalance(d));

export const setWalletAction = createAction(SET_WALLET,
  async (d) => await setUserWallet(d));

export const getEthRateAction = createAction(GET_ETH_RATE,
  async () => await getEthRate());

export const getUserAction = createAction(GET_USER,
  async (d) => await getUser());

export const getCurrentPriceAction = createAction(GET_CURRENT_PRICE,
  async (d) => await getCurrentPrice());

export const getEthRaisedAction = createAction(GET_ETH_RAISED,
  async (d) => await getEthRaised());

export const getProgressAction = createAction(GET_PROGRESS,
  async (d) => await getProgress());

export const getHistoryAction = createAction(GET_HISTORY,
  async (d) => await getHistory(d));
export const getDatesAction = createAction(GET_DATES,
  async (d) => await getDates(d));

export const getReferralInfoAction = createAction(GET_REFERRAL_INFO,
  async (d) => await getReferralInfo(d));