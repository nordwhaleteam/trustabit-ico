import React from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import MainMenu from 'app/components/MainMenu';
import FlexColumn from 'common/FlexColumn';
import immutableProps from 'app/hocs/immutableProps';

const MainContainer = styled(FlexColumn)`
`;

class AppContainer extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  static propTypes = {
    children: PropTypes.node,
  };

  render() {
    return (
      <MainContainer>
        <MainMenu {...this.props}/>
        {React.Children.toArray(this.props.children)}
      </MainContainer>

    );
  }
}

const mapStateToProps = (state) => ({
  user: state.getIn(['app', 'user']),
});
const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(
  immutableProps(AppContainer));