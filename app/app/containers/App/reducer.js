import typeToReducer from 'type-to-reducer';
import {fromJS} from 'immutable';
import getUser from 'app/utils/getUser';
import reducerParse from '../../utils/reducerParse';
import {SET_WALLET} from 'app/containers/DashBoardContainer/constants';
import setUser from 'app/utils/setUser';

const initialState = fromJS({
  user: fromJS(getUser()),
});

export default typeToReducer({
  [SET_WALLET]: {
    SUCCESS: (state = fromJS([]), d) => reducerParse(d, (data) => {
        setUser(data);
        return state.set('user',fromJS(data));
      }
      ,
      (payload) => {
        const {data, status} = payload;
        return state;
      },
    ),
  },

}, initialState);

