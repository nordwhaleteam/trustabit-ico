import {gasPrice, getContract, setProvider, web3} from '../client';
import contractSettings from 'blockchain-config/ICO.json';
import {icoAddress, provider} from 'blockchain-config/eth';
import erc20 from './erc20';

let instance;
export default class ico {

  constructor(contract) {
    this._contract = contract;
  }

  static async instance() {
    if (!instance) {
      instance = await ico.create();
    }
    return instance;
  }

  static async create() {
    await setProvider(provider);
    const contract = await getContract(contractSettings.abi, icoAddress);
    return new ico(contract);
  }

  async getGasPrice() {
    return gasPrice();
  }

  async getCurrentPrice() {
    const price = await this._contract.methods.getCurrentPrice().call();
    return web3.utils.fromWei(price);
  }

  async getEthRaised() {
    const raised = await this._contract.methods.weiRaised().call();
    return web3.utils.fromWei(raised);
  }

  async getTokenRaised() {
    const raised = await this._contract.methods.getTokenRaised().call();
    return this.fromTokenDecimals(raised);
  }

  async fromTokenDecimals(amount) {
    const token = await erc20.instance();
    return token.fromTokenDecimals(amount);
  }

  async getAvailableToken() {
    return this._contract.methods.AVAILABLE_TOKENS().call();
  }

  async getTokenAdvisoryBountyTeam() {
    return this._contract.methods.tokenAdvisoryBountyTeam().call();
  }

  async preSaleStartDate() {
    return (await this._contract.methods.preSaleStartDate().call()) * 1000;
  }

  async preSaleEndDate() {
    return (await this._contract.methods.preSaleEndDate().call()) * 1000;
  }

  async mainSaleStartDate() {
    return (await this._contract.methods.mainSaleStartDate().call()) * 1000;
  }

  async mainSaleEndDate() {
    return (await this._contract.methods.mainSaleEndDate().call()) * 1000;
  }

  getHistory = (filter) => new Promise((resolve, reject) => {
    this._contract.getPastEvents('TokenPurchase', {
      filter,
      fromBlock: 0,
      toBlock: 'latest',
    }, async (err, events) => {
      if (events && events.length > 0) {
        const result = [];
        for (const e of events) {
          const {value, amount} = e.returnValues;
          const block = await web3.eth.getBlock(e.blockNumber);
          result.push({
            eth: web3.utils.fromWei(value),
            token: await this.fromTokenDecimals(amount),
            date: new Date(block.timestamp * 1000),
          });
        }

        return resolve(result);
      }
      return resolve([]);
    });
  });

}