import {GAS_VALUE, getContract, sendTx, setProvider, toHex} from '../client';
import contractSettings from '../abi/ERC20.json';
import {tokenContractAddress, provider} from 'blockchain-config/eth';

let instance;
export default class Erc20 {

  constructor(contract) {
    this._contract = contract;
    this._decimals = undefined;
  }

  static async instance() {
    if (!instance) {
      instance = await Erc20.create();
    }
    return instance;
  }

  static async create() {
    await setProvider(provider);
    const contract = await getContract(contractSettings.abi,
      tokenContractAddress);
    return new Erc20(contract);
  }

  async getDecimals() {
    if (!this._decimals) {
      this._decimals = this._contract.methods.decimals().call() || 0;
    }
    return this._decimals;
  }

  async getDecimalsMulti() {
    return Math.pow(10, await this.getDecimals());
  }

  async fromTokenDecimals(tokens) {
    const decimals = await this.getDecimalsMulti();
    return tokens / decimals;
  }

  async balanceOf(address) {
    const decimals = await this.getDecimalsMulti();
    const balance = await this._contract.methods.balanceOf(address).call();
    return balance / decimals;
  }

  async transfer(wallet, address, amount) {
    const decimals = await this.getDecimalsMulti();
    address = toHex(address);
    amount = parseInt(amount * 100, 10) / 100 * decimals;
    return await sendTx(wallet, this._contract.methods.transfer, GAS_VALUE,
      address,
      amount);
  }
}
