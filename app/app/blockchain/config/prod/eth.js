export const tokenContractAddress = '0x4ddaf983302f451a5a6dc95af102335bf280d9a7';
export const icoAddress = '0x39Db2AB26F405916f0020ca12bAA95e56fADF599';

export const provider = 'https://mainnet.infura.io';
export const providerScan = 'https://mainnet.etherscan.io/tx';

