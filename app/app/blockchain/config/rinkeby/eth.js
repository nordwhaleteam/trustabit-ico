export const tokenContractAddress = '0x539912ebdfa78fba72d5e962ec458e1e600f105d';
export const icoAddress = '0xd58c92411afddb98a88666b4f14ddea7e8f30e7f';

export const provider = 'https://rinkeby.infura.io';
export const providerScan = 'https://rinkeby.etherscan.io/tx';