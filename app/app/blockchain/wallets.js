import Util from 'ethereumjs-util';
import Wallet from 'ethereumjs-wallet';

let _oracleWallet;

export const getWalletByPk = (_pk) => {
    if (!_oracleWallet) {
        const pk = Util.toBuffer(_pk);
        _oracleWallet = Wallet.fromPrivateKey(pk);
    }

    return _oracleWallet;
};
