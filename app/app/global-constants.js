import { create } from 'apisauce';
import getUser from 'app/utils/getUser';
import logOut from './utils/logOut';

const PROD_API = 'https://tokensale.trustabit.io';
const STAGE_API = 'http://trustabit.ico.dev.nordwhale.com';
 const LOCAL_API = 'http://localhost:4244';
//const LOCAL_API = 'http://trustabit.ico.dev.nordwhale.com';
const apiV1 = 'api';
export const isLocal = location.hostname === 'localhost';
export const isStage = location.hostname.indexOf('trustabit.ico.dev.nordwhale.com') !== -1;
export const HOST = isLocal ? 'http://localhost:5000' : (isStage ? STAGE_API : PROD_API);
export const getApiUrl = () => isLocal ? LOCAL_API : (isStage ? STAGE_API : PROD_API);

export const getHeaderConfig = (token) => ({
  Accept: 'application/json',
  'Accept-Language': 'ru',
  Authorization: token && `Bearer ${token}`,
});


const apiMonitor = (response) => {
  if (response.data && response.data === 'Unauthorized') {
    logOut()
  }
};

export const apiCreate = () => {
  const user = getUser();
  const api = create({
    baseURL: `${getApiUrl()}/${apiV1}`,
    headers: getHeaderConfig(user && user.token),
  });
  api.addMonitor(apiMonitor);
  return api;
};