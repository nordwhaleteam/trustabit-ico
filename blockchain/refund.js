const TruffleWalletProvider = require('truffle-wallet-provider');
const {rinkebyWallet,rinkebyOracle,prodDeployer} = require('./wallets');
const Web3 = require('web3');
const TrustaBitCrowdsale = require('./build/contracts/TrustaBitCrowdsale');

//prod
const wallet = prodDeployer;
const providerUrl = 'https://mainnet.infura.io';


//rinkeby
// const wallet = rinkebyWallet
// const providerUrl = 'https://rinkeby.infura.io'

//local
// const wallet = rinkebyOracle
// const providerUrl = 'http://localhost:8545'

const provider = new TruffleWalletProvider(wallet, providerUrl);
const web3 = new Web3(new Web3.providers.HttpProvider(providerUrl));

const contract = require('truffle-contract');

const ICO_ADDRESS = '0x39Db2AB26F405916f0020ca12bAA95e56fADF599';

const crowdsale = contract(TrustaBitCrowdsale);
crowdsale.setProvider(provider);
const refund = async () => {
  try {
    const instance = await crowdsale.at(ICO_ADDRESS);
    console.log ('softcap  ',web3.fromWei(await instance.softCap.call()).toNumber())
    console.log ('ethRaised',web3.fromWei(await instance.weiRaised.call()).toNumber())
    console.log ('getBalance',web3.fromWei(await web3.eth.getBalance(wallet.getAddressString())).toNumber())
  // const res = await instance.refund({from:wallet.getAddressString(),gas:4700000});
   // console.log(res)
   // console.log(res.receipt.logs)
  //  console.log ('getBalance',(await web3.eth.getBalance(wallet.getAddressString())).toNumber())
  }catch (e){
    console.log(e);
  }
};

refund();