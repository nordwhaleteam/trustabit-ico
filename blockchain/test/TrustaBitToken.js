const Web3 = require('web3');
const web3 = new Web3();
const TrustaBitToken = artifacts.require('./TrustaBitToken.sol');

contract('TrustaBitToken', (accounts) => {

  let newContract;

  it('new Contract, mint to accounts[1]', () => {
    return TrustaBitToken.new()
      .then(async (instance) => {
        newContract = instance;
        await instance.mint(accounts[1], web3.toWei(20));

        assert.equal(await instance.balanceOf(accounts[1]),web3.toWei(20))
      })
  });

  it('balanceOf accounts[1]', () => {
    return Promise.resolve(newContract)
      .then((instance) => instance.balanceOf.call(accounts[1]))
      .then(res => {
        assert.equal(res.toNumber(), web3.toWei(20));
      });
  });

  it('transfer to accounts[0] before finishminting', () => {
    return Promise.resolve(newContract)
      .then((instance) => instance.transfer(accounts[0], web3.toWei(10), {from: accounts[1]}))
      .catch(err => {
        assert.notEqual(String(err && err.message).search('revert'), -1);
      });
  });
  it('finishMinting', () => {
    return Promise.resolve(newContract)
      .then(async (instance) => {
        assert.equal(await instance.mintingFinished(),false);
        await instance.finishMinting()
        assert.equal(await instance.mintingFinished(),true);
      })
  });



  it('transfer to accounts[0] after finishminting', () => {
    return Promise.resolve(newContract)
      .then(async (instance)=>{
        console.log('mintingFinished',await instance.mintingFinished())
        console.log('accounts[0]',accounts[0],+await instance.balanceOf(accounts[0]))
        console.log('accounts[1]',accounts[1],+await instance.balanceOf(accounts[1]))
        await instance.transfer(accounts[0], web3.toWei(10), {from: accounts[1]})
        assert.equal(+await instance.balanceOf(accounts[0]),web3.toWei(10));
      })
  });

  it('balanceOf accounts[0]', () => {
    return Promise.resolve(newContract)
      .then((instance) => instance.balanceOf.call(accounts[0]))
      .then(res => {
        assert.equal(res.toNumber(), web3.toWei(10));
      });
  });

});
