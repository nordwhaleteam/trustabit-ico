pragma solidity 0.4.18;

import "zeppelin-solidity/contracts/math/SafeMath.sol";

contract MilestoneCrowdsale {

  using SafeMath for uint256;

  /* Number of available tokens */
  uint256 public constant AVAILABLE_TOKENS = 1e9; //1 billion

  /* Total Tokens available in PreSale */
  uint256 public constant AVAILABLE_IN_PRE_SALE = 40e6; // 40,000,000

  /* Total Tokens available in Main ICO */
  uint256 public constant AVAILABLE_IN_MAIN = 610e6; // 610,000,000;

  /* Early Investors token available */
  uint256 public constant AVAILABLE_FOR_EARLY_INVESTORS = 100e6; // 100,000,000;

  /* Pre-Sale Start Date */
  uint public preSaleStartDate;

  /* Pre-Sale End Date */
  uint public preSaleEndDate;

  /* Main Token Sale Date */
  uint public mainSaleStartDate;

  /* Main Token Sale End */
  uint public mainSaleEndDate;

  struct Milestone {
    uint start; // UNIX timestamp
    uint end; // UNIX timestamp
    uint256 bonus;
    uint256 price;
  }

  Milestone[] public milestones;

  uint256 public rateUSD; // (cents)

  uint256 public earlyInvestorTokenRaised;
  uint256 public preSaleTokenRaised;
  uint256 public mainSaleTokenRaised;


  function initMilestones(uint _rate, uint _preSaleStartDate, uint _preSaleEndDate, uint _mainSaleStartDate, uint _mainSaleEndDate) internal {
    rateUSD = _rate;
    preSaleStartDate = _preSaleStartDate;
    preSaleEndDate = _preSaleEndDate;
    mainSaleStartDate = _mainSaleStartDate;
    mainSaleEndDate = _mainSaleEndDate;

    /**
     * Early investor Milestone
     * Prise: $0.025 USD (2.5 cent)
     * No bonuses
     */
    uint256 earlyInvestorPrice = uint(25 ether).div(rateUSD.mul(10));
    milestones.push(Milestone(0, preSaleStartDate, 0, earlyInvestorPrice));

    /**
     * Pre-Sale Milestone
     * Prise: $0.05 USD (5 cent)
     * Bonus: 20%
     */
    uint256 preSalePrice = usdToEther(5);
    milestones.push(Milestone(preSaleStartDate, preSaleEndDate, 20, preSalePrice));

    /**
     * Main Milestones
     * Prise: $0.10 USD (10 cent)
     * Week 1 Bonus: 15%
     * Week 2 Main Token Sale Bonus: 10%
     * Week 3 Main Token Sale Bonus: 5%
     */
    uint256 mainSalePrice = usdToEther(10);
    uint mainSaleStartDateWeek1 = mainSaleStartDate.add(1 weeks);
    uint mainSaleStartDateWeek3 = mainSaleStartDate.add(3 weeks);
    uint mainSaleStartDateWeek2 = mainSaleStartDate.add(2 weeks);

    milestones.push(Milestone(mainSaleStartDate, mainSaleStartDateWeek1, 15, mainSalePrice));
    milestones.push(Milestone(mainSaleStartDateWeek1, mainSaleStartDateWeek2, 10, mainSalePrice));
    milestones.push(Milestone(mainSaleStartDateWeek2, mainSaleStartDateWeek3, 5, mainSalePrice));
    milestones.push(Milestone(mainSaleStartDateWeek3, _mainSaleEndDate, 0, mainSalePrice));
  }

  function usdToEther(uint256 usdValue) public view returns (uint256) {
    // (usdValue * 1 ether / rateUSD)
    return usdValue.mul(1 ether).div(rateUSD);
  }

  function getCurrentMilestone() internal view returns (uint256, uint256) {
    for (uint i = 0; i < milestones.length; i++) {
      if (now >= milestones[i].start && now < milestones[i].end) {
        var milestone = milestones[i];
        return (milestone.bonus, milestone.price);
      }
    }

    return (0, 0);
  }

  function getCurrentPrice() public view returns (uint256) {
    var (, price) = getCurrentMilestone();

    return price;
  }

  function getTokenRaised() public view returns (uint256) {
    return mainSaleTokenRaised.add(preSaleTokenRaised.add(earlyInvestorTokenRaised));
  }

  function isEarlyInvestors() public view returns (bool) {
    return now < preSaleStartDate;
  }

  function isPreSale() public view returns (bool) {
    return now >= preSaleStartDate && now < preSaleEndDate;
  }

  function isMainSale() public view returns (bool) {
    return now >= mainSaleStartDate && now < mainSaleEndDate;
  }

  function isEnded() public view returns (bool) {
    return now >= mainSaleEndDate;
  }

}
