pragma solidity 0.4.18;


import "zeppelin-solidity/contracts/math/SafeMath.sol";
import 'zeppelin-solidity/contracts/crowdsale/RefundVault.sol';
import "../Token/TrustaBitToken.sol";
import "./MilestoneCrowdsale.sol";


contract TrustaBitCrowdsale is MilestoneCrowdsale, Ownable {

  using SafeMath for uint256;

  /* Minimum contribution */
  uint public constant MINIMUM_CONTRIBUTION = 15e16;

  /* Soft cap */
  uint public constant softCapUSD = 3e6; //$3 Million USD
  uint public softCap; //$3 Million USD in ETH

  /* Hard Cap */
  uint public constant hardCapUSD = 49e6; //$49 Million USD
  uint public hardCap; //$49 Million USD in ETH

  /* Advisory Bounty Team */
  address public addressAdvisoryBountyTeam;
  uint256 public constant tokenAdvisoryBountyTeam = 250e6;

  address[] public investors;

  TrustaBitToken public token;

  address public wallet;

  uint256 public weiRaised;

  RefundVault public vault;

  bool public isFinalized = false;

  event Finalized();

  /**
   * event for token purchase logging
   * @param investor who got the tokens
   * @param value weis paid for purchase
   * @param amount amount of tokens purchased
   */
  event TokenPurchase(address indexed investor, uint256 value, uint256 amount);

  modifier hasMinimumContribution() {
    require(msg.value >= MINIMUM_CONTRIBUTION);
    _;
  }

  function TrustaBitCrowdsale(address _wallet, address _token, uint _rate, uint _preSaleStartDate, uint _preSaleEndDate, uint _mainSaleStartDate, uint _mainSaleEndDate, address _AdvisoryBountyTeam) public {
    require(_token != address(0));
    require(_AdvisoryBountyTeam != address(0));
    require(_rate > 0);
    require(_preSaleStartDate > 0);
    require(_preSaleEndDate > 0);
    require(_preSaleEndDate > _preSaleStartDate);
    require(_mainSaleStartDate > 0);
    require(_mainSaleStartDate >= _preSaleEndDate);
    require(_mainSaleEndDate > 0);
    require(_mainSaleEndDate > _mainSaleStartDate);

    wallet = _wallet;
    token = TrustaBitToken(_token);
    addressAdvisoryBountyTeam = _AdvisoryBountyTeam;

    initMilestones(_rate, _preSaleStartDate, _preSaleEndDate, _mainSaleStartDate, _mainSaleEndDate);

    softCap = usdToEther(softCapUSD.mul(100));
    hardCap = usdToEther(hardCapUSD.mul(100));

    vault = new RefundVault(wallet);
  }

  function investorsCount() public view returns (uint) {
    return investors.length;
  }

  // fallback function can be used to buy tokens
  function() external payable {
    buyTokens(msg.sender);
  }

  // low level token purchase function
  function buyTokens(address investor) public hasMinimumContribution payable {
    require(investor != address(0));
    require(!isEnded());

    uint256 weiAmount = msg.value;

    require(getCurrentPrice() > 0);

    uint256 tokensAmount = calculateTokens(weiAmount);
    require(tokensAmount > 0);

    mintTokens(investor, weiAmount, tokensAmount);
    increaseRaised(weiAmount, tokensAmount);

    if (vault.deposited(investor) == 0) {
      investors.push(investor);
    }
    // send ether to the fund collection wallet
    vault.deposit.value(weiAmount)(investor);
  }

  function calculateTokens(uint256 weiAmount) internal view returns (uint256) {
    if ((weiRaised.add(weiAmount)) > hardCap) return 0;

    var (bonus, price) = getCurrentMilestone();

    uint256 tokensAmount = weiAmount.div(price).mul(10 ** token.decimals());
    tokensAmount = tokensAmount.add(tokensAmount.mul(bonus).div(100));

    if (isEarlyInvestorsTokenRaised(tokensAmount)) return 0;
    if (isPreSaleTokenRaised(tokensAmount)) return 0;
    if (isMainSaleTokenRaised(tokensAmount)) return 0;
    if (isTokenAvailable(tokensAmount)) return 0;

    return tokensAmount;
  }

  function isEarlyInvestorsTokenRaised(uint256 tokensAmount) public view returns (bool) {
    return isEarlyInvestors() && (earlyInvestorTokenRaised.add(tokensAmount) > AVAILABLE_FOR_EARLY_INVESTORS.mul(10 ** token.decimals()));
  }

  function isPreSaleTokenRaised(uint256 tokensAmount) public view returns (bool) {
    return isPreSale() && (preSaleTokenRaised.add(tokensAmount) > AVAILABLE_IN_PRE_SALE.mul(10 ** token.decimals()));
  }

  function isMainSaleTokenRaised(uint256 tokensAmount) public view returns (bool) {
    return isMainSale() && (mainSaleTokenRaised.add(tokensAmount) > AVAILABLE_IN_MAIN.mul(10 ** token.decimals()));
  }

  function isTokenAvailable(uint256 tokensAmount) public view returns (bool) {
    return getTokenRaised().add(tokensAmount) > AVAILABLE_TOKENS.mul(10 ** token.decimals());
  }

  function increaseRaised(uint256 weiAmount, uint256 tokensAmount) internal {
    weiRaised = weiRaised.add(weiAmount);

    if (isEarlyInvestors()) {
      earlyInvestorTokenRaised = earlyInvestorTokenRaised.add(tokensAmount);
    }

    if (isPreSale()) {
      preSaleTokenRaised = preSaleTokenRaised.add(tokensAmount);
    }

    if (isMainSale()) {
      mainSaleTokenRaised = mainSaleTokenRaised.add(tokensAmount);
    }
  }

  function mintTokens(address investor, uint256 weiAmount, uint256 tokens) internal {
    token.mint(investor, tokens);
    TokenPurchase(investor, weiAmount, tokens);
  }

  function finalize() onlyOwner public {
    require(!isFinalized);
    require(isEnded());

    if (softCapReached()) {
      vault.close();
      mintAdvisoryBountyTeam();
      token.finishMinting();
    }
    else {
      vault.enableRefunds();
      token.finishMinting();
    }

    token.transferOwnership(owner);

    isFinalized = true;
    Finalized();
  }

  function mintAdvisoryBountyTeam() internal {
    mintTokens(addressAdvisoryBountyTeam, 0, tokenAdvisoryBountyTeam.mul(10 ** token.decimals()));
  }

  // if crowdsale is unsuccessful, investors can claim refunds here
  function claimRefund() public {
    require(isFinalized);
    require(!softCapReached());

    vault.refund(msg.sender);
  }

  function refund() onlyOwner public {
    require(isFinalized);
    require(!softCapReached());

    for (uint i = 0; i < investors.length; i++) {
      address investor = investors[i];
      if (vault.deposited(investor) != 0) {
        vault.refund(investor);
      }
    }
  }

  function softCapReached() public view returns (bool) {
    return weiRaised >= softCap;
  }

  function hardCapReached() public view returns (bool) {
    return weiRaised >= hardCap;
  }

  function destroy() onlyOwner public {
    selfdestruct(owner);
  }
}
