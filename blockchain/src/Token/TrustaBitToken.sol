pragma solidity 0.4.18;


import "zeppelin-solidity/contracts/token/MintableToken.sol";

/**
 * @title SimpleToken
 * @dev Very simple ERC20 Token example, where all tokens are pre-assigned to the creator.
 * Note they can later distribute these tokens as they wish using `transfer` and other
 * `StandardToken` functions.
 */
contract TrustaBitToken is MintableToken {

  string public constant name = "TrustaBits";

  string public constant symbol = "TAB";

  uint256 public constant decimals = 18;

  modifier isFinishedMinting () {
    require(mintingFinished);
    _;
  }
  function transfer(address _to, uint256 _value) public isFinishedMinting returns (bool) {
    return super.transfer(_to, _value);
  }

  function transferFrom(address _from, address _to, uint256 _value) public isFinishedMinting returns (bool) {
    return super.transferFrom(_from, _to, _value);
  }

  function approve(address _spender, uint256 _value) public isFinishedMinting returns (bool) {
    return super.approve(_spender, _value);
  }

  function increaseApproval(address _spender, uint _addedValue) public isFinishedMinting returns (bool success) {
    return super.increaseApproval(_spender, _addedValue);
  }

  function decreaseApproval(address _spender, uint _subtractedValue) public isFinishedMinting returns (bool success) {
    return super.decreaseApproval(_spender, _subtractedValue);
  }

}