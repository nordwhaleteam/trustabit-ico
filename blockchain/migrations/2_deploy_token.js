const fs = require('fs')
const path = require('path')

const TrustaBitToken = artifacts.require('./TrustaBitToken')

module.exports = function (deployer, network, accounts) {
  const args = {}
  switch (network) {
    case 'local':
      args.wallet = accounts[0]
      args.total = 1000000000e18
      break
    case 'rinkeby':
      const {rinkebyWallet} = require('../wallets')
      args.wallet = rinkebyWallet.getAddressString()
      console.log('wallet',rinkebyWallet.getAddressString(),rinkebyWallet.getPrivateKeyString())
      args.total = 1000000000e18
      break
    default:
      args.wallet = '0xbD939BAcb078430d3630721A621c0e56fB036337'
      args.total = 1000000000e18
  }

  deployContract(deployer, args)
    .then(() => {
      console.log('DONE')
    })
    .catch(e => console.log(e))
}

const deployContract = async (deployer, {
  wallet,total
}) => {

  await deployer.deploy(TrustaBitToken)
  console.log(`export const tokenContractAddress = '${TrustaBitToken.address}';`)
  const contract = await TrustaBitToken.deployed()

  console.log('mint '+ total +' TrustaBitToken')
  await contract.mint(wallet,total)
  //
  console.log('finish minting')
  await contract.finishMinting()
  //
  // console.log('release')
  // await contract.release()
  console.log('change TrustaBitToken transferOwnership to', wallet)
  await contract.transferOwnership(wallet)

}