const TruffleWalletProvider = require('truffle-wallet-provider');
const {rinkebyWallet,rinkebyOracle} = require('./wallets');
const TrustaBitCrowdsale = require('./build/contracts/TrustaBitCrowdsale');
const Web3 = require('web3');

//rinkeby
const wallet = rinkebyWallet
const providerUrl = 'https://rinkeby.infura.io'
//local
// const wallet = rinkebyOracle
// const providerUrl = 'http://localhost:8545'

const provider = new TruffleWalletProvider(wallet, providerUrl);
const web3 = new Web3(new Web3.providers.HttpProvider(providerUrl));

const contract = require('truffle-contract');

const ICO_ADDRESS = '0xb8b1ff5faf555cbd6da206da915a9c56eea4aa6f';

const crowdsale = contract(TrustaBitCrowdsale);
crowdsale.setProvider(provider);
const finalize = async () => {
  try {
    const instance = await crowdsale.at(ICO_ADDRESS);
    console.log ('isFinalized  ',(await instance.isFinalized.call()))
    console.log ('softcap  ',(await instance.softCap.call()).toNumber())
    console.log ('weiRaised',(await instance.weiRaised.call()).toNumber())
    console.log ('isEarlyInvestors',(await instance.isEarlyInvestors.call()))
    console.log ('isPreSale',(await instance.isPreSale.call()))
    console.log ('isMainSale',(await instance.isMainSale.call()))
    console.log ('hasEnded',(await instance.isEnded.call()))
    console.log ('getBalance',(await web3.eth.getBalance(wallet.getAddressString())).toNumber())
    const res = await instance.finalize({from:wallet.getAddressString(),gas:4700000});
    console.log ('getBalance',(await web3.eth.getBalance(wallet.getAddressString())).toNumber())
    console.log(res)
    console.log(res.receipt.logs)
  }catch (e){
    console.log(e);
  }
};

finalize();