#!/bin/bash

PROJECT="trustabit-ico"
FILE="deploy.prod.tar.gz"

sudo rm -rf /var/www/trustabit-ico/public
sudo rm -rf /var/www/trustabit-ico/app
sudo rm -rf /var/www/trustabit-ico/api
sudo rm -f /var/www/deploy.tar.gz

read -p "Enter filename to download from bitbucket, default [${FILE}] " NEWFILE
[ -n "${NEWFILE}" ] && FILE=$NEWFILE

read -p "Enter bitbucket login " USER
URL="https://bitbucket.org/nordwhaleteam/${PROJECT}/downloads/${FILE}"
sudo curl -L -su "${USER}" "${URL}" -o deploy.tar.gz

sudo mkdir public
sudo tar -xf ./deploy.tar.gz -C ./public
sudo rm -rf /var/www/trustabit-ico/app
sudo mv ./public/app/build /var/www/trustabit-ico/app

if [ -f /var/www/trustabit-ico/api/pm2-prod.json ] ; then
    sudo pm2 stop /var/www/trustabit-ico/api/pm2-prod.json
fi

if [ -f /var/www/trustabit-ico/api/pm2-cron-prod.json ] ; then
    sudo pm2 stop /var/www/trustabit-ico/api/pm2-cron-prod.json
fi

sudo mv ./public/api /var/www/trustabit-ico/api
sudo cp config.json /var/www/trustabit-ico/api/config/config.json
sudo cp .env /var/www/trustabit-ico/api/.env

sudo rm -rf /var/www/trustabit-ico/public
cd ./public
sudo rm -rf api app blockchain
cd ..
sudo mv public /var/www/trustabit-ico/public

cd /var/www/trustabit-ico/api
sudo pm2 start pm2-prod.json
sudo pm2 start pm2-cron-prod.json

sudo node_modules/.bin/sequelize db:migrate

sudo rm -f /var/www/deploy.tar.gz

curl -X POST --data-urlencode "payload={\"channel\": \"#trustabit_ico\", \"text\": \"${USER} deployed ${FILE}. Please check <https://tokensale.trustabit.io>\"}" https://hooks.slack.com/services/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


