var IcoContract = (function() {
  var _this;

  /**
   * Correct Progress: quick at start - slow at finish.
   * NOTE: Newer reach 100%
   * @param {number} value - current value of progress;
   * @param {number} max - max value of progress;
   * @return {number} - percent of progress [0-100]%
   */
  function correctProgress(value, max) {
    value = +value || 0;
    max = +max || 1;
    var x = Math.floor(value * 100 / max);
    return Math.floor((1 - Math.pow(0.96, x)) * 100);
  }
  function IcoContract() {
    _this = this;
    _this.address = '0x39Db2AB26F405916f0020ca12bAA95e56fADF599';
    //  _this.provider = 'http://localhost:8545';
    _this.provider = 'https://mainnet.infura.io';
    _this.abi = [
      {
        'constant': true,
        'inputs': [],
        'name': 'investorsCount',
        'outputs': [{'name': '', 'type': 'uint256'}],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function',
      },
      {
        'constant': true,
        'inputs': [],
        'name': 'softCapReached',
        'outputs': [{'name': '', 'type': 'bool'}],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function',
      },
      {
        'constant': true,
        'inputs': [],
        'name': 'weiRaised',
        'outputs': [{'name': '', 'type': 'uint256'}],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function',
      },
      {
        'constant': true,
        'inputs': [],
        'name': 'mainSaleEndDate',
        'outputs': [{'name': '', 'type': 'uint256'}],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function',
      },
      {
        'constant': true,
        'inputs': [],
        'name': 'isPreSale',
        'outputs': [{'name': '', 'type': 'bool'}],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function',
      },
      {
        'constant': true,
        'inputs': [],
        'name': 'preSaleStartDate',
        'outputs': [{'name': '', 'type': 'uint256'}],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function',
      },
      {
        'constant': true,
        'inputs': [],
        'name': 'mainSaleStartDate',
        'outputs': [{'name': '', 'type': 'uint256'}],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function',
      },
      {
        'constant': true,
        'inputs': [],
        'name': 'isEarlyInvestors',
        'outputs': [{'name': '', 'type': 'bool'}],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function',
      },
      {
        'constant': true,
        'inputs': [],
        'name': 'getTokenRaised',
        'outputs': [{'name': '', 'type': 'uint256'}],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function',
      },
      {
        'constant': true,
        'inputs': [],
        'name': 'preSaleEndDate',
        'outputs': [{'name': '', 'type': 'uint256'}],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function',
      },
      {
        'constant': true,
        'inputs': [],
        'name': 'AVAILABLE_TOKENS',
        'outputs': [{'name': '', 'type': 'uint256'}],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function',
      },
      {
        'constant': true,
        'inputs': [],
        'name': 'isFinalized',
        'outputs': [{'name': '', 'type': 'bool'}],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function',
      },
      {
        'constant': true,
        'inputs': [],
        'name': 'softCap',
        'outputs': [{'name': '', 'type': 'uint256'}],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function',
      },
      {
        'constant': true,
        'inputs': [],
        'name': 'preSaleTokenRaised',
        'outputs': [{'name': '', 'type': 'uint256'}],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function',
      },
      {
        'constant': true,
        'inputs': [],
        'name': 'hardCapReached',
        'outputs': [{'name': '', 'type': 'bool'}],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function',
      },
      {
        'constant': true,
        'inputs': [],
        'name': 'releaseTokenDate',
        'outputs': [{'name': '', 'type': 'uint256'}],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function',
      },
      {
        'constant': true,
        'inputs': [],
        'name': 'isEnded',
        'outputs': [{'name': '', 'type': 'bool'}],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function',
      },
      {
        'constant': true,
        'inputs': [],
        'name': 'mainSaleTokenRaised',
        'outputs': [{'name': '', 'type': 'uint256'}],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function',
      },
      {
        'constant': true,
        'inputs': [],
        'name': 'isMainSale',
        'outputs': [{'name': '', 'type': 'bool'}],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function',
      },
      {
        'constant': true,
        'inputs': [],
        'name': 'earlyInvestorTokenRaised',
        'outputs': [{'name': '', 'type': 'uint256'}],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function',
      },
      {
        'constant': true,
        'inputs': [],
        'name': 'hardCap',
        'outputs': [{'name': '', 'type': 'uint256'}],
        'payable': false,
        'stateMutability': 'view',
        'type': 'function',
      },
    ];
    _this.web3 = new Web3(new Web3.providers.HttpProvider(_this.provider));
    _this.instance = _this.web3.eth.contract(_this.abi).at(_this.address);
    _this.data = {};
  }

  IcoContract.prototype.fromDecimals = function(value) {
    return Math.floor(parseInt(value) / Math.pow(10, 18));
  };
  IcoContract.prototype.getEthUsdRate = function(cb) {
    $.get('https://api.coinmarketcap.com/v1/ticker/ethereum/', function(d) {
      if (d && d[0] && d[0].price_usd) {
        _this.data.ethUsedRate = d && d[0] && d[0].price_usd && (+d[0].price_usd);
        _this.checkData(cb);
      }
    });
  };

  IcoContract.prototype.getEthRaised = function(cb) {
    _this.instance.weiRaised.call(function(err, res) {
      _this.data.ethRaised = _this.web3.fromWei(res).toNumber();
      _this.checkData(cb);
    });
  };
  IcoContract.prototype.checkData = function(cb) {
    if (typeof _this.data.ethRaised !== 'undefined' &&
        typeof _this.data.ethUsedRate !== 'undefined' &&
        typeof _this.data.preSaleStartDate !== 'undefined' &&
        typeof _this.data.preSaleEndDate !== 'undefined' &&
        typeof _this.data.mainSaleStartDate !== 'undefined' &&
        typeof _this.data.mainSaleEndDate !== 'undefined' &&
        typeof _this.data.tokenRaised !== 'undefined' &&
        typeof _this.data.availableToken !== 'undefined'
    ) {
      // _this.data.preSaleStartDate = Math.floor(((+new Date()) + 5 * 1000));
      // _this.data.preSaleEndDate = Math.floor(((+new Date()) + 10 * 1000));
      // _this.data.mainSaleStartDate = Math.floor(((+new Date()) + 15 * 1000));
      // _this.data.mainSaleEndDate = Math.floor(((+new Date()) + 20 * 1000));
      cb(_this.data);
    }
  };

  IcoContract.prototype.preSaleStartDate = function(cb) {
    _this.instance.preSaleStartDate.call(function(err, res) {
      _this.data.preSaleStartDate = res * 1000;
      _this.checkData(cb);
    });
  };
  IcoContract.prototype.preSaleEndDate = function(cb) {
    _this.instance.preSaleEndDate.call(function(err, res) {
      _this.data.preSaleEndDate = res * 1000;
      _this.checkData(cb);
    });
  };
  IcoContract.prototype.mainSaleStartDate = function(cb) {
    _this.instance.mainSaleStartDate.call(function(err, res) {
      _this.data.mainSaleStartDate = res * 1000;
      _this.checkData(cb);
    });
  };
  IcoContract.prototype.mainSaleEndDate = function(cb) {
    _this.instance.mainSaleEndDate.call(function(err, res) {
      _this.data.mainSaleEndDate = res * 1000;
      _this.checkData(cb);
    });
  };

  IcoContract.prototype.getTokenRaised = function(cb) {
    _this.instance.getTokenRaised.call(function(err, res) {
      _this.data.tokenRaised = _this.web3.fromWei(res).toNumber();
      _this.checkData(cb);
    });
  };
  IcoContract.prototype.getAvailableToken = function(cb) {
    _this.instance.AVAILABLE_TOKENS.call(function(err, res) {
      _this.data.availableToken = res.toNumber();
      _this.checkData(cb);
    });
  };
  IcoContract.prototype.getIcoData = function(cb) {
    _this.getEthRaised(cb);
    _this.getEthUsdRate(cb);
    _this.preSaleStartDate(cb);
    _this.preSaleEndDate(cb);
    _this.mainSaleStartDate(cb);
    _this.mainSaleEndDate(cb);
    _this.getTokenRaised(cb);
    _this.getAvailableToken(cb);
  };
  IcoContract.prototype.isFinalized = function(cb) {
    _this.instance.isFinalized.call(function(err, res) {
      _this.data.finalize = res;
    });
  };

  IcoContract.prototype.getCurrentTitle = function() {
    var now = +new Date();
    if (now < _this.data.preSaleStartDate) {
      return 'TrustaBit Pre-Sale Starts in';
    }
    if (now < _this.data.preSaleEndDate) {
      return 'TrustaBit Pre-Sale Ends in';
    }
    if (now < _this.data.mainSaleStartDate) {
      return 'TrustaBit Main-Sale Starts in';
    }
    if (now < _this.data.mainSaleEndDate) {
      return 'TrustaBit Main-Sale Ends in';
    }
    return 'TrustaBit ICO Finished';
  };

  IcoContract.prototype.getCurrentDeadline = function() {
    var now = +new Date();
    if (now < _this.data.preSaleStartDate) {
      return _this.data.preSaleStartDate;
    }
    if (now < _this.data.preSaleEndDate) {
      return _this.data.preSaleEndDate;
    }
    if (now < _this.data.mainSaleStartDate) {
      return _this.data.mainSaleStartDate;
    }
    if (now < _this.data.mainSaleEndDate) {
      return _this.data.mainSaleEndDate;
    }
    return 0;
  };

  IcoContract.prototype.getCurrentBonus = function() {
    var now = +new Date();
    if (now < _this.data.preSaleStartDate) {
      return 0;
    }
    if (now < _this.data.preSaleEndDate) {
      return 20;
    }
    if (now < _this.data.mainSaleStartDate) {
      return 0;
    }
    if (now < _this.data.mainSaleStartDate + 7 * 24 * 60 * 60 * 1000) { //1st week
      return 15
    }
    if (now < _this.data.mainSaleStartDate + 14 * 24 * 60 * 60 * 1000) {//2nd week
      return 10
    }
    if (now < _this.data.mainSaleStartDate + 21 * 24 * 60 * 60 * 1000) {//3d week
      return 5
    }
    return 0;
  };
  IcoContract.prototype.getProgress = function() {
    // console.log('r',_this.data.tokenRaised)
    // console.log('a',_this.data.availableToken)
    if (_this.data.availableToken > 0) {
      return correctProgress(_this.data.tokenRaised, _this.data.availableToken);
    }
    return 0;
  };

  return IcoContract;
})();

var Timer = (function() {
  var _this;

  function Timer() {
    _this = this;
  }

  Timer.prototype.getTimeRemaining = function() {
    var t = Date.parse(new Date(_this.endtime)) - Date.parse(new Date());
    return {
      total: t,
      days: Math.floor(t / (1000 * 60 * 60 * 24)) || '00',
      hours: Math.floor((t / (1000 * 60 * 60)) % 24) || '00',
      minutes: Math.floor((t / 1000 / 60) % 60) || '00',
      seconds: Math.floor((t / 1000) % 60) || '00',
    };
  };
  Timer.prototype.updateClock = function() {
    var t = _this.getTimeRemaining();
    _this.onInit();
    _this.daysSpan.innerHTML = t.days;
    _this.hoursSpan.innerHTML = ('0' + t.hours).slice(-2);
    _this.minutesSpan.innerHTML = ('0' + t.minutes).slice(-2);
    _this.secondsSpan.innerHTML = ('0' + t.seconds).slice(-2);

    if (t.total <= 0) {
      clearInterval(_this.timeinterval);
      _this.initializeClock(_this.clockId, _this.ico, _this.onInit);
    }
  };

  Timer.prototype.initializeClock = function(id, ico, onInit) {
    _this.clockId = id;
    _this.onInit = onInit;
    _this.ico = ico;
    _this.clock = document.getElementById(id);
    _this.endtime = ico.getCurrentDeadline();
    _this.onInit();
    if (+new Date < _this.endtime) {
      _this.daysSpan = _this.clock.querySelector('.days');
      _this.hoursSpan = _this.clock.querySelector('.hours');
      _this.minutesSpan = _this.clock.querySelector('.minutes');
      _this.secondsSpan = _this.clock.querySelector('.seconds');
      _this.updateClock();
      _this.timeinterval = setInterval(_this.updateClock, 1000);
    } else {
      $(_this.clock).hide();
    }
  };

  return Timer;
})();
var priceFormatString = function(v) {
  v = v || '';
  v = v.toString();
  var index = v.indexOf('.');
  if (index !== -1) {
    v = v.substr(0, index) + v.substr(index, 3);
  }
  return v;
};
var icoContract = new IcoContract();
var icoTimer = new Timer();

function getProgressPath(w) {
  return 'M' + w + ',1 L1,1 L1,16 L' + w + ',16 C' + w + ',10.4494185 ' + w + ',5.4494185 ' + w + ',1 Z';
}

function setProgressBar(percent) {
  var $progressBar = $('.scale-it');
  var $plane = $('#plane');
  var val = percent * 9;
  $progressBar.attr('d', getProgressPath(val));
  $plane.attr('transform', 'translate(' + (percent*7.75) + ',0)');

}

icoContract.getIcoData(function(data) {
  $('#contributed-usd').html(priceFormatString(data.ethRaised * data.ethUsedRate) + ' USD');
  $('#timer-title').html(icoContract.getCurrentTitle());

  icoTimer.initializeClock('clockdiv', icoContract, function() {
    $('#timer-title').html(icoContract.getCurrentTitle());
    var bonus = icoContract.getCurrentBonus();
    $('.bonus-item').removeClass('current-bonus-item');
    $('#bonus-' + bonus).addClass('current-bonus-item');
  });

  setProgressBar(icoContract.getProgress());
});

setInterval(function() {
  icoContract.getTokenRaised(function(data) {
    setProgressBar(icoContract.getProgress());
  });
}, 120 * 1000);

